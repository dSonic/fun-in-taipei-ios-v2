//
//  NotificationViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class NotificationViewController: UIViewController, UITableViewDelegate,
    UITableViewDataSource {
    
    private var pleaseSignInVC: PleaseSigninViewController!
    private var noContentVC: NoContentViewController!
    private var notifications: [[String : AnyObject]] = []
    private var isNextPage = false
    private var nextPageUrl = ""
    private var poiId: Int!
    private var poiDetailInfo: NSDictionary!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        cleanUpViews()
        
        let authToken = Auth.sharedAuth.authToken
        
        // Checking if user is logged in
        // Switching views accordingly
        if authToken.isEmpty == true {
            tableView.hidden = true
            
            pleaseSignInVC = self.storyboard?.instantiateViewControllerWithIdentifier("PleaseSigninViewController") as! PleaseSigninViewController
            pleaseSignInVC.view.frame = self.view.frame
            self.addChildViewController(pleaseSignInVC)
            self.view.insertSubview(pleaseSignInVC.view, atIndex: 0)
            pleaseSignInVC!.didMoveToParentViewController(self)
        } else {
            getNotifications()
            
            if notifications.isEmpty {
                tableView.hidden = true
                
                noContentVC = self.storyboard?.instantiateViewControllerWithIdentifier("NoContentVC") as! NoContentViewController
                noContentVC.view.frame = self.view.frame
                self.addChildViewController(noContentVC)
                self.view.insertSubview(noContentVC.view, atIndex: 0)
                noContentVC!.didMoveToParentViewController(self)
            } else {
                tableView.hidden = false
            }
        }
    }
    
    private func cleanUpViews() {
        if pleaseSignInVC != nil {
            pleaseSignInVC.willMoveToParentViewController(nil)
            pleaseSignInVC.view.removeFromSuperview()
            pleaseSignInVC.removeFromParentViewController()
        }
        
        if noContentVC != nil {
            noContentVC.willMoveToParentViewController(nil)
            noContentVC.view.removeFromSuperview()
            noContentVC.removeFromParentViewController()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.backgroundImage = UIImage(named: "tabBar_notification")
        
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "通知列表"
        
        drawSlideMenuButton()
    }
    
    func drawSlideMenuButton() {
        let addImage: UIImage? = UIImage(named: "slide_menu_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        
        let slideMenuButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        if self.revealViewController() != nil {
            addButton.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
            self.view.addGestureRecognizer((self.revealViewController().panGestureRecognizer()))
        }
        
        self.navigationItem.setLeftBarButtonItem(slideMenuButton, animated: false)
    }
    
    private func getNotifications() {
        let sharedAuth = Auth.sharedAuth
        let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        let deviceId = sharedAuth.apnsToken
        let apiUrl = ApiList.sharedApi.getNotificationApi(deviceId)
        
        let response = Just.get(apiUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            let jsonData = response.json as! NSDictionary
            let data = jsonData.objectForKey("data") as! [String : AnyObject]
            let status = jsonData.objectForKey("status") as! Int

            if status == 200 {
                notifications = data["results"] as! [[String : AnyObject]]
                
                let nextPage = data["next"]
                
                if let _ = nextPage as? NSNull {
                    isNextPage = false
                } else {
                    isNextPage = true
                    nextPageUrl = nextPage as! String
                }
                
                tableView.reloadData()
            } else if status == 500 {
                notifications = []
            }
            
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    private func getMoreNotifications() {
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        
        let response = Just.get(nextPageUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            let data = response.json?.objectForKey("data") as! [String : AnyObject]
            let moreNotifications = data["results"] as! [[String : AnyObject]]
            
            for notification in moreNotifications {
                notifications.append(notification)
            }
            
            let nextPage = data["next"]
            
            if let _ = nextPage as? NSNull {
                isNextPage = false
            } else {
                isNextPage = true
                nextPageUrl = nextPage as! String
            }
            
            tableView.reloadData()
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NotificationCell", forIndexPath: indexPath) as! NotificationTableViewCell
        
        cell.borderView.layer.borderColor = UIColor.blackColor().CGColor
        cell.borderView.layer.borderWidth = 1.0
        
        let createTime = notifications[indexPath.row]["create_time"] as? String
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        let createTimeObj = formatter.dateFromString(createTime!)
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let createTimeStr = formatter.stringFromDate(createTimeObj!)
        
        let thumbnailUrl = notifications[indexPath.row]["thumbnail"] as? String
        
        let sharedPoi = PoiModel.sharedPoiModel
        let poiId = notifications[indexPath.row]["uid"] as! Int
        let message = sharedPoi.getNotificationMessageByPoiId(poiId)
        cell.messageLabel.text = message
        cell.messageTimeLabel.text = createTimeStr
        cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbnailUrl!)!)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 96.0
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // When user scroll to the bottom of the screen,
        // trigger the getMoreBookmarks method to get more bookmarks
        // if isNextPage == true
        if self.tableView.contentOffset.y >= self.tableView.contentSize.height - self.tableView.bounds.size.height {
            if isNextPage == true {
                isNextPage = false
                
                getMoreNotifications()
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "NotificationToPoiDetailSegue" {
            let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
            
            poiDetailVC.poiDetailInfo = poiDetailInfo
            poiDetailVC.poiId = poiId
            poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "NotificationToPoiDetailSegue" {
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            let selectedIndex = tableView.indexPathForSelectedRow?.row
            poiId = notifications[selectedIndex!]["uid"] as! Int
            
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getPoiDetailApi(poiId)
            
            let response = Just.get(apiUrl, headers: ["Authorization": authToken])
            
            if response.ok {
                poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
                
                return true
            } else if response.statusCode == nil {
                let alert = Alert()
                alert.noConnectionAlert()
                
                return false
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
                
                return false
            }
        }
        return true
    }
}
