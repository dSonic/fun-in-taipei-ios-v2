//
//  BookmarkTableViewCell.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/9/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class BookmarkTableViewCell: UITableViewCell {
    
    @IBOutlet weak var poiThumbImage: UIImageView!
    @IBOutlet weak var poiTypeIcon: UIImageView!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var poiMovieLabel: UILabel!
    @IBOutlet weak var poiAddressLabel: UILabel!
    @IBOutlet weak var borderView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
