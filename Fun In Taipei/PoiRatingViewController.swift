//
//  PoiRatingViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just
import Haneke

class PoiRatingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PoiRatingEditorContentDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var poiId = 0
    var poiDetailInfo: NSDictionary!
    var ratings = []
    var ratingImages: [UIImage] = []
    let moods = ["", "開心", "好玩", "驚喜", "很酷", "滿意", "普通", "無聊",
    "失望", "想哭", "生氣"]
    let moodIcons = [UIImage(), UIImage(named: "rating_mood_happy"),
    UIImage(named: "rating_mood_fun"), UIImage(named: "rating_mood_surprise"),
    UIImage(named: "rating_mood_cool"), UIImage(named: "rating_mood_satisfy"),
    UIImage(named: "rating_mood_normal"), UIImage(named: "rating_mood_boring"),
    UIImage(named: "rating_mood_upset"), UIImage(named: "rating_mood_cry"),
    UIImage(named: "rating_mood_angry")]
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchRatings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTableView()
    }
    
    private func fetchRatings() {
        let apiUrl = ApiList.sharedApi.getRatingEditingApi(poiId)
        
        let response = Just.get(apiUrl)
        
        if response.ok {
            let data = response.json as! NSDictionary
            ratings = data.objectForKey("results") as! [[String : AnyObject]]
            
            tableView.reloadData()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }

    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 160.0
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratings.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // In order to use Auto Layout for TableView cells, 
        // we need to return 2 cells for the one with image and without.
        let imageUrlStr = ratings[indexPath.row]["image"] as! String
        let nickName = ratings[indexPath.row]["nickname"] as! String
        let nickNameText = "\(nickName) 覺得"
        let mood = moods[ratings[indexPath.row]["mood"] as! Int]
        let moodIcon = moodIcons[ratings[indexPath.row]["mood"] as! Int]
        
        let createTime = ratings[indexPath.row]["create_time"] as? String
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        let createTimeObj = formatter.dateFromString(createTime!)
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        let createTimeStr = formatter.stringFromDate(createTimeObj!)
        
        if imageUrlStr.isEmpty == true {
            let cell = tableView.dequeueReusableCellWithIdentifier("PoiRatingBasicCell", forIndexPath: indexPath) as! PoiRatingBasicCell
            
            cell.nickNameLabel.text = nickNameText
            cell.moodImageView.image = moodIcon
            cell.moodLabel.text = mood
            cell.timeLabel.text = createTimeStr
            cell.messageLabel.text = ratings[indexPath.row]["message"] as? String
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("PoiRatingImageCell", forIndexPath: indexPath) as! PoiRatingImageCell
            
            let imageUrl = NSURL(string: imageUrlStr)
            
            cell.nickNameLabel.text = nickNameText
            cell.moodImageView.image = moodIcon
            cell.moodLabel.text = mood
            cell.timeLabel.text = createTimeStr
            cell.ratingImageView.hnk_setImageFromURL(imageUrl!)
            cell.messageLabel.text = ratings[indexPath.row]["message"] as? String
            
            return cell
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiRatingToEditorSegue" {
            let ratingEditorVC = segue.destinationViewController as! PoiRatingEditorViewController
            
            ratingEditorVC.poiId = poiId
            ratingEditorVC.poiDetailInfo = poiDetailInfo
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "PoiRatingToEditorSegue" {
            let sharedAuth = Auth.sharedAuth
            
            if sharedAuth.authToken.isEmpty == false {
                return true
            } else {
                presentSigninAlertView()
                return false
            }
        }
        
        return true
    }
    
    /**
     * This function to show the Login Alert View
     * if the user has pressed the bookmark button without login.
     */
    func presentSigninAlertView() {
        let alertController = UIAlertController(title: "溫馨提醒",
            message: "此功能需要登入後才能使用...", preferredStyle: .Alert)
        let signInAction = UIAlertAction(title: "立即登入", style: .Destructive, handler: {action in
            let authVC = self.storyboard?.instantiateViewControllerWithIdentifier("AuthRootViewController") as! AuthRootViewController
            self.presentViewController(authVC, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "暫不登入", style: .Cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(signInAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func reloadRatingsTable() {
        fetchRatings()
    }
    
    func contentHeightOfContainer(height: CGFloat) {
        // do nothing
    }
}
