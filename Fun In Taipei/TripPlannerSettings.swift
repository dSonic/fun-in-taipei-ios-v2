//
//  TripPlannerSettings.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/15/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation

class TripPlannerSettings {
    class var sharedTripPlannerSettings: TripPlannerSettings {
        struct Singleton {
            static let instance = TripPlannerSettings()
        }
        
        return Singleton.instance
    }
    
    private(set) var settings: NSDictionary
    
    init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let storedSettings = defaults.objectForKey("trip_planner_settings") as? NSDictionary
        
        let initalSetting = [
            "season": 0,
            "period": -1,
            "weather": 0
        ]
        
        settings = storedSettings != nil ? storedSettings! : initalSetting
    }
    
    func updateSettings(settings: NSDictionary) {
        self.settings = settings
        saveSettings()
    }
    
    func resetSettings() {
        settings = [
            "season": 0,
            "period": -1,
            "weather": 0
        ]
        saveSettings()
    }
    
    private func saveSettings() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.settings, forKey: "trip_planner_settings")
        defaults.synchronize()
    }
}