//
//  DateExtension.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation

extension NSDate
{
    convenience
    init(dateString:String) {
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let d = dateStringFormatter.dateFromString(dateString)!
        self.init(timeInterval:0, sinceDate:d)
    }
}