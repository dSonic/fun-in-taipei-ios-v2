//
//  TripPlannerFilterViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class TripPlannerFilterViewController: UIViewController, CLLocationManagerDelegate, TripPlannerSeasonViewDelegate, TripPlannerPeriodDelegate, TripPlannerWeatherDelegate {
    
    @IBOutlet weak var contentVC: TripPlannerContentViewController!
    @IBOutlet weak var seasonCircleImage: UIImageView!
    @IBOutlet weak var periodCircleImage: UIImageView!
    @IBOutlet weak var weatherCircleImage: UIImageView!
    @IBOutlet weak var seasonButton: UIButton!
    @IBOutlet weak var periodButton: UIButton!
    @IBOutlet weak var weatherButton: UIButton!
    
    let locationManager = CLLocationManager()
    var myLocation:CLLocationCoordinate2D! = nil
    var tourList: NSDictionary!
    var days: Int!
    
    private var resultVC: TripPlannerResultTableTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "行程規劃"
        
        drawConfirmButton()
        drawCancelButton()
        
        confirmButtonToggle()
        
        seasonButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            locationManager.stopUpdatingLocation()
            self.myLocation = location.coordinate
        }
    }
    
    func drawConfirmButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_confirm_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "confirmButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        let sharedTripPlannerSettings = TripPlannerSettings.sharedTripPlannerSettings
        sharedTripPlannerSettings.resetSettings()
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func confirmButtonPressed(sender: UIButton) {
        let sharedTripPlannerSettings = TripPlannerSettings.sharedTripPlannerSettings
        
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getApiUrl("trip_advisor")
        
        if myLocation != nil {
            let longitude = NSString(format: "%.07f", myLocation.longitude)
            let latitude = NSString(format: "%.07f", myLocation.latitude)
            let season = sharedTripPlannerSettings.settings["season"] as! Int
            days = sharedTripPlannerSettings.settings["period"] as! Int
            let mood = sharedTripPlannerSettings.settings["weather"] as! Int
            
            let response = Just.post(
                apiUrl,
                data: [
                    "longitude": longitude,
                    "latitude": latitude,
                    "season": season,
                    "days": days,
                    "mood": mood
                ]
            )
            
            if response.ok {
                self.tourList = response.json?.objectForKey("data") as! NSDictionary
                self.performSegueWithIdentifier("TripPlannerResultSegue", sender: nil)
            } else if response.statusCode == nil {
                let alert = Alert()
                alert.noConnectionAlert()
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
            }
        }
    }
    
    @IBAction func periodButtonPressed(sender: UIButton) {
        seasonButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        seasonCircleImage.hidden = true
        weatherButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        weatherCircleImage.hidden = true
        
        periodButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        periodCircleImage.hidden = false
        self.contentVC.swapToViewControllerWithIdentifier("TripPlannerPeriodSegue")
    }
    
    @IBAction func seasonButtonPressed(sender: UIButton) {
        periodButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        periodCircleImage.hidden = true
        weatherButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        weatherCircleImage.hidden = true
        
        seasonButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        seasonCircleImage.hidden = false
        
        self.contentVC.swapToViewControllerWithIdentifier("TripPlannerFilterSeasonSegue")
    }
    
    @IBAction func weatherButtonPressed(sender: UIButton) {
        periodButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        periodCircleImage.hidden = true
        seasonButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        seasonCircleImage.hidden = true
        
        weatherButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        weatherCircleImage.hidden = false
        
        self.contentVC.swapToViewControllerWithIdentifier("TripPlannerWeatherSegue")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TripPlannerContentSegue" {
            self.contentVC = segue.destinationViewController as! TripPlannerContentViewController
            self.contentVC.seasonDelegate = self
            self.contentVC.periodDelegate = self
            self.contentVC.weatherDelegate = self
            
        } else if segue.identifier == "TripPlannerResultSegue" {
            resultVC = segue.destinationViewController as! TripPlannerResultTableTableViewController
            resultVC.tourList = tourList
            resultVC.days = days
        }
    }
    
    // Season View Delegate Method
    func moveFromSeasonToDays() {
        seasonButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        seasonCircleImage.hidden = true
        weatherButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        weatherCircleImage.hidden = true
        
        periodButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        periodCircleImage.hidden = false
        self.contentVC.swapToViewControllerWithIdentifier("TripPlannerPeriodSegue")
    }
    
    func confirmButtonToggleFromSeason() {
        confirmButtonToggle()
    }
    
    // Period View Delegate Method
    func moveFromDaysToWeather() {
        periodButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        periodCircleImage.hidden = true
        seasonButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        seasonCircleImage.hidden = true
        
        weatherButton.setTitleColor(UIColor.redColor(), forState: .Normal)
        weatherCircleImage.hidden = false
        
        self.contentVC.swapToViewControllerWithIdentifier("TripPlannerWeatherSegue")
    }
    
    func confirmButtonToggleFromPeriod() {
        confirmButtonToggle()
    }
    
    // Weather View Delegate Method
    func confirmButtonToggleFromWeather() {
        confirmButtonToggle()
    }
    
    private func confirmButtonToggle() {
        let sharedTirpPlannerSettings = TripPlannerSettings.sharedTripPlannerSettings
        let settings = sharedTirpPlannerSettings.settings
        let season = settings.objectForKey("season") as! Int
        let period = settings.objectForKey("period") as! Int
        let weather = settings.objectForKey("weather") as! Int
        
        if season == 0 || period == -1 || weather == 0 {
            self.navigationItem.rightBarButtonItem?.enabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
    }
}
