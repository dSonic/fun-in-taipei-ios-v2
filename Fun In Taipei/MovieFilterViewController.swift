//
//  MovieFilterViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/17/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

protocol MovieFilterDelegate {
    func updateMovieListByFilter(movieList: Array<NSDictionary>)
}

class MovieFilterViewController: UIViewController {

    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var comedyButton: UIButton!
    @IBOutlet weak var fictionButon: UIButton!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var scaryButton: UIButton!
    
    private var isLoveSelected = false
    private var isComedySelected = false
    private var isFictionSelected = false
    private var isActionSelected = false
    private var isHistorySelected = false
    private var isScarySelected = false
    
    private var typeButtonImage = UIImage(named: "movie_filter_type_btn")
    private var typeButtonImagePressed = UIImage(named: "movie_filter_type_btn_pressed")
    private var darkColor = UIColor.darkGrayColor()
    private var whiteColor = UIColor.whiteColor()
    
    var delegate: MovieFilterDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loveButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        comedyButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        fictionButon.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        actionButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        historyButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        scaryButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        
        drawCancelButton()
        drawConfirmButton()
        
        confirmButtonToggle()
    }
    
    func drawConfirmButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_confirm_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "confirmButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func confirmButtonPressed(sender: UIButton) {
        // Generating query string for API call
        var queryString = ""
        
        if isLoveSelected == true {
            queryString += "1,"
        }
        
        if isComedySelected == true {
            queryString += "2,"
        }
        
        if isFictionSelected == true {
            queryString += "3,"
        }
        
        if isActionSelected == true {
            queryString += "4,"
        }
        
        if isHistorySelected == true {
            queryString += "5,"
        }
        
        if isScarySelected == true {
            queryString += "6,"
        }
        
        let index = queryString.endIndex.advancedBy(-1)
        let newQueryString = queryString.substringToIndex(index)
        
        let apiUrl = ApiList.sharedApi.getMovieFilterApi(newQueryString)
        
        let response = Just.get(apiUrl)
        
        if response.ok {
            let data = response.json?.objectForKey("data") as! Array<NSDictionary>
            delegate.updateMovieListByFilter(data)
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func loveButtonPressed(sender: UIButton) {
        isLoveSelected = !isLoveSelected
        let buttonImage = isLoveSelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isLoveSelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func comedyButtonPressed(sender: UIButton) {
        isComedySelected = !isComedySelected
        let buttonImage = isComedySelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isComedySelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func fictionButtonPressed(sender: UIButton) {
        isFictionSelected = !isFictionSelected
        let buttonImage = isFictionSelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isFictionSelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func actionButtonPressed(sender: UIButton) {
        isActionSelected = !isActionSelected
        let buttonImage = isActionSelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isActionSelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func historyButtonPressed(sender: UIButton) {
        isHistorySelected = !isHistorySelected
        let buttonImage = isHistorySelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isHistorySelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func scaryButtonPressed(sender: UIButton) {
        isScarySelected = !isScarySelected
        let buttonImage = isScarySelected == true ? typeButtonImagePressed : typeButtonImage
        let buttonColor = isScarySelected == true ? whiteColor : darkColor
        sender.setBackgroundImage(buttonImage, forState: .Normal)
        sender.setTitleColor(buttonColor, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    private func confirmButtonToggle() {
        if (isLoveSelected == true || isComedySelected == true || isFictionSelected == true
            || isActionSelected == true || isHistorySelected == true || isScarySelected == true) {
            self.navigationItem.rightBarButtonItem?.enabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
    }
}
