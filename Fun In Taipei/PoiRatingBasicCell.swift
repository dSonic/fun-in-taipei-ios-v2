//
//  PoiRatingBasicCell.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/27/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PoiRatingBasicCell: UITableViewCell {
    @IBOutlet weak var nickNameLabel: UILabel!
    @IBOutlet weak var moodImageView: UIImageView!
    @IBOutlet weak var moodLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
