//
//  PoiIntroViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Haneke

class PoiIntroViewController: UIViewController, PoiIntroContentDelegate {
    
    var poiDetailInfo: NSDictionary!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentVC: PoiIntroContentViewController!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        // Set the content size of the scroll view
        // vary on the height of the textview in the container view
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiIntroContentSegue" {
            self.contentVC = segue.destinationViewController as! PoiIntroContentViewController
            self.contentVC.poiDetailInfo = self.poiDetailInfo
            contentVC.delegate = self
        }
    }

    func resizeContentSize(contentHeight: CGFloat) {
        scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, contentHeight)
    }
}
