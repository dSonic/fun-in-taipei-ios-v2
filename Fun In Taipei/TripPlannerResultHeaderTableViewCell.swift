//
//  TripPlannerResultHeaderTableViewCell.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/15/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class TripPlannerResultHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
