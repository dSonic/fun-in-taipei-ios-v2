//
//  FeedbackContentViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/28/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import SwiftValidator
import Just

protocol FeedbackContentDelegate {
    func scrollviewContentHeight(height: CGFloat)
}

class FeedbackContentViewController: UIViewController, UITextFieldDelegate, ValidationDelegate, UITextViewDelegate {

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var nameErrorLabel: UILabel!
    @IBOutlet weak var messageErrorLabel: UILabel!
    
    var delegate: FeedbackContentDelegate! = nil
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        messageTextView.layer.borderWidth = 0.5
        
        validator.registerField(emailTextField, errorLabel: emailErrorLabel,
            rules: [RequiredRule(message: "請輸入Eamil"), EmailRule(message: "無效的Email")])
        validator.registerField(nameTextField, errorLabel: nameErrorLabel,
            rules: [RequiredRule(message: "請輸入姓名"), MaxLengthRule(length: 30, message: "長度不能大於25")])
    }
    
    override func viewDidLayoutSubviews() {
        // call delegate method to resize the scrollview content
        // based on the send button position
        let yPos = sendButton.frame.origin.y
        let buttonHeight = sendButton.frame.size.height
        let finalHeight = yPos + buttonHeight + 10
        
        delegate.scrollviewContentHeight(finalHeight)
    }

    override func viewDidAppear(animated: Bool) {
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func sendButtonPressed(sender: UIButton) {
        emailTextField.layer.borderColor = UIColor.blackColor().CGColor
        emailTextField.layer.borderWidth = 0.1
        emailErrorLabel.hidden = true
        
        nameTextField.layer.borderColor = UIColor.blackColor().CGColor
        nameTextField.layer.borderWidth = 0.1
        nameErrorLabel.hidden = true
        
        messageTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        messageTextView.layer.borderWidth = 0.5
        messageErrorLabel.hidden = true
        
        validator.validate(self)
    }
    
    func validationSuccessful() {
        if messageTextView.text.isEmpty == true {
            messageTextView.layer.borderColor = UIColor.redColor().CGColor
            messageTextView.layer.borderWidth = 1.0
            messageErrorLabel.hidden = false
        } else {
            sendMessage()
        }
    }
    
    func sendMessage() {
        let apiUrl = ApiList.sharedApi.getApiUrl("feedback")
        let email = emailTextField.text!
        let name = nameTextField.text!
        let feedback = messageTextView.text!
        
        let params: [String : AnyObject] = [
            "email": email,
            "name": name,
            "feedback": feedback
        ]
        
        let response = Just.post(
            apiUrl,
            data: params
        )
        
        if response.ok {
            let alert = UIAlertView(title: "送出成功", message: "您的訊息已成功送出，謝謝您寶貴的意見...",
                delegate: self, cancelButtonTitle: "確認")
            alert.show()
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in validator.errors {
            field.layer.borderColor = UIColor.redColor().CGColor
            field.layer.borderWidth = 1.0
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.hidden = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == emailTextField {
            emailTextField.resignFirstResponder()
            nameTextField.becomeFirstResponder()
        }
        
        if textField == nameTextField {
            nameTextField.resignFirstResponder()
            messageTextView.becomeFirstResponder()
        }
        
        return true
    }
    
}
