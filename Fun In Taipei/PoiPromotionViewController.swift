//
//  PoiPromotionViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import QRCodeReader
import Just

class PoiPromotionViewController: UIViewController, PoiPromotionDelegate, QRCodeReaderViewControllerDelegate, PoiCouponDelegate {
    private var poiPromotionScrollVC: PoiPromotionScrollViewController!
    private var poiPromotionContentVC: PoiPromotionContentViewController!
    private var poiCouponVC: PoiCouponViewController!
    
    lazy var reader = QRCodeReaderViewController(cancelButtonTitle: "取消")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        poiPromotionScrollVC = self.storyboard?.instantiateViewControllerWithIdentifier("PoiPromotionScrollViewController") as! PoiPromotionScrollViewController
        poiPromotionScrollVC.delegate = self
        
        poiPromotionScrollVC.view.frame = self.view.frame
        self.addChildViewController(poiPromotionScrollVC)
        self.view.insertSubview(poiPromotionScrollVC.view, atIndex: 0)
        poiPromotionScrollVC.didMoveToParentViewController(self)
    }
    
    /// Resize the content size of the ScrollView 
    /// base on the y origin of the get coupon button 
    /// on the Poi Promotion Content View
    func resizeContentSize(height: CGFloat) {
        poiPromotionScrollVC.scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /// PoiPromotionDelegate method to show the QRCode Reader
    /// when user press the get coupon button.
    ///
    func showQRCodeReader() {
        reader.delegate = self
        
        poiPromotionScrollVC.willMoveToParentViewController(nil)
        poiPromotionScrollVC.view.removeFromSuperview()
        poiPromotionScrollVC.removeFromParentViewController()
        
        reader.view.frame.size.width = self.view.frame.size.width
        reader.view.frame.size.height = self.view.frame.size.height - 10
        self.addChildViewController(reader)
        self.view.insertSubview(reader.view, atIndex: 0)
        reader.didMoveToParentViewController(self)
    }
    
    /// Reader Delegate method
    ///
    /// It can be used for getting scan resutl string.
    ///
    /// - parameters:
    ///   - reader: A `QRCodeReaderViewController`
    ///   - result: A `String` of the QRCdoe scan result
    ///
    func reader(reader: QRCodeReaderViewController, didScanResult result: String) {
        let couponId = Int(result)!
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getCouponApi(couponId)
        let authToken = Auth.sharedAuth.authToken
        
        let response = Just.get(apiUrl, headers: ["Authorization": "JWT \(authToken)"])
        
        if response.ok {
            let couponImageUrl = response.json?.objectForKey("data")?.objectForKey("coupon_image") as! String
            
            poiCouponVC = self.storyboard?.instantiateViewControllerWithIdentifier("PoiCouponViewController") as! PoiCouponViewController
            poiCouponVC.couponImageUrl = couponImageUrl
            poiCouponVC.delegate = self
            
            swapFromViewController(reader, toViewController: poiCouponVC)
        } else {
            let alert = Alert()
            alert.noConnectionAlert()
        }
    }
    
    /// A method to close the QRCode Reader
    /// when user has clicked the cancel button.
    func readerDidCancel(reader: QRCodeReaderViewController) {
        swapFromViewController(reader, toViewController: poiPromotionScrollVC)
    }
    
    /// A private method to switch between views
    ///
    /// - parameters:
    ///   - fromViewController: A `UIViewController` to switch from
    ///   - fromViewController: A `UIviewController` to switch to
    private func swapFromViewController(fromViewController:UIViewController, toViewController:UIViewController) {
        fromViewController.willMoveToParentViewController(nil)
        fromViewController.view.removeFromSuperview()
        fromViewController.removeFromParentViewController()
        
        toViewController.view.frame = self.view.frame
        self.addChildViewController(toViewController)
        self.view.insertSubview(toViewController.view, atIndex: 0)
        toViewController.didMoveToParentViewController(self)
    }
    
    /// A delegate method to go back to the Store Promotion View 
    /// when you have clicked on the back button on the Coupon View.
    func closeCouponView() {
        swapFromViewController(poiCouponVC, toViewController: poiPromotionScrollVC)
    }

}
