//
//  TripPlannerContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class TripPlannerContentViewController: UIViewController {
    private var seasonVC: TripPlannerSeasonViewController!
    var currentIdentifier: String!
    var seasonDelegate: TripPlannerSeasonViewDelegate! = nil
    var periodDelegate: TripPlannerPeriodDelegate! = nil
    var weatherDelegate: TripPlannerWeatherDelegate! = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentIdentifier = "TripPlannerFilterSeasonSegue"
        self.performSegueWithIdentifier(currentIdentifier, sender: nil)
    }
    
    private func swapFromViewController(fromViewController:UIViewController, toViewController:UIViewController) {
        fromViewController.willMoveToParentViewController(nil)
        fromViewController.view.removeFromSuperview()
        fromViewController.removeFromParentViewController()
        
        toViewController.view.frame = self.view.frame
        self.addChildViewController(toViewController)
        self.view.insertSubview(toViewController.view, atIndex: 0)
        toViewController.didMoveToParentViewController(self)
    }
    
    func swapToViewControllerWithIdentifier(identifier: String) {
        currentIdentifier = identifier
        self.performSegueWithIdentifier(currentIdentifier, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TripPlannerFilterSeasonSegue" {
            if self.childViewControllers.count > 0 {
                let tripPlannerSeasonVC = segue.destinationViewController as! TripPlannerSeasonViewController
                tripPlannerSeasonVC.delegate = self.seasonDelegate
                
                self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
            } else {
                let tripPlannerSeasonVC = segue.destinationViewController as! TripPlannerSeasonViewController
                tripPlannerSeasonVC.delegate = self.seasonDelegate
                
                segue.destinationViewController.view.frame = self.view.frame
                self.addChildViewController(segue.destinationViewController)
                self.view.insertSubview(segue.destinationViewController.view, atIndex: 0)
                segue.destinationViewController.didMoveToParentViewController(self)
            }
        } else if segue.identifier == "TripPlannerPeriodSegue" {
            let periodVC = segue.destinationViewController as! TripPlannerPeriodViewController
            periodVC.delegate = self.periodDelegate
            
            self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
        } else if segue.identifier == "TripPlannerWeatherSegue" {
            let weatherVC = segue.destinationViewController as! TripPlannerWeatherViewController
            weatherVC.delegate = weatherDelegate
            
            self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
        }
    }
}
