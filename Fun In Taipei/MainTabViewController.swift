//
//  MainTabViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/10/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        let itemTag = item.tag
        
        switch itemTag {
            case 1:
                tabBar.backgroundImage = UIImage(named: "tabBar_map")
            case 2:
                tabBar.backgroundImage = UIImage(named: "tabBar_movie")
            case 3:
                tabBar.backgroundImage = UIImage(named: "tabBar_trip_planner")
            case 4:
                tabBar.backgroundImage = UIImage(named: "tabBar_bookmark")
            case 5:
                tabBar.backgroundImage = UIImage(named: "tabBar_notification")
            default:
                tabBar.backgroundImage = UIImage(named: "tabBar_map")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
