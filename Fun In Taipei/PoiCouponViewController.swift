//
//  PoiCouponViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/8/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol PoiCouponDelegate {
    func closeCouponView()
}

class PoiCouponViewController: UIViewController {
    var couponImageUrl: String!
    
    @IBOutlet weak var couponImageView: UIImageView!
    
    var delegate: PoiCouponDelegate! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        couponImageView.layer.borderColor = UIColor(netHex: 0xdbd2c6).CGColor
        couponImageView.layer.borderWidth = 2.0
        couponImageView.hnk_setImageFromURL(NSURL(string: couponImageUrl)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonPressed(sender: UIButton) {
        delegate.closeCouponView()
    }
    
    @IBAction func saveButtonPressed(sender: UIButton) {
        UIImageWriteToSavedPhotosAlbum(couponImageView.image!, self, "image:didFinishSavingWithError:contextInfo:", nil)
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if error == nil {
            delegate.closeCouponView()
            let alert = UIAlertView(title: "儲存成功", message: "優惠卷已儲存到您的相簿裡...", delegate: self, cancelButtonTitle: "確認")
            alert.show()
        } else {
            delegate.closeCouponView()
            let alert = UIAlertView(title: "儲存失敗", message: "請再試一次...", delegate: self, cancelButtonTitle: "確認")
            alert.show()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
