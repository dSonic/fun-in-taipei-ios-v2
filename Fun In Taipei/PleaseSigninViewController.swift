//
//  PleaseSigninViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PleaseSigninViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signinButtonPressed(sender: UIButton) {
        let authRootVC = self.storyboard?.instantiateViewControllerWithIdentifier("AuthRootViewController") as! AuthRootViewController
        
        self.parentViewController?.presentViewController(authRootVC, animated: true, completion: nil)
    }

}
