//
//  FeedbackViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/28/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController, FeedbackContentDelegate {

    @IBOutlet var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "意見回饋"
        
        drawCancelButton()
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let cancelBarButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(cancelBarButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "FeedbackContentSegue" {
            let feedbackContentVC = segue.destinationViewController as! FeedbackContentViewController
            feedbackContentVC.delegate = self
        }
    }

    func scrollviewContentHeight(height: CGFloat) {
        scrollView.contentSize.height = height
    }
}
