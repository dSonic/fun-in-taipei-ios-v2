//
//  AppDelegate.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/10/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Alamofire
import SQLite
import IQKeyboardManagerSwift
import Just

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let googleMapsApiKey = "AIzaSyAoexZ4cyIjMcxGzhoHUgfTIXDsXBiAtWg"
    
    var sharedLocation: Location!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.

        UINavigationBar.appearance().setBackgroundImage(UIImage(named: "navigationbar_back.png"), forBarMetrics: .Default)
        
        GMSServices.provideAPIKey(googleMapsApiKey)
        
        // Getting API Calls host url
        let apiList = ApiList.sharedApi
        apiList.setUrlHost()
        
        // Saving POIs to the database
        let poiModel = PoiModel.sharedPoiModel
        poiModel.generateData()
        
        IQKeyboardManager.sharedManager().enable = true
        
        UIApplication.sharedApplication().registerForRemoteNotifications()
        let settings = UIUserNotificationSettings(forTypes: .Sound, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)

        let notificationType = UIUserNotificationType.Sound
        let localSettings = UIUserNotificationSettings(forTypes: notificationType, categories: nil)
        application.registerUserNotificationSettings(localSettings)
        
        sharedLocation = Location.sharedLocation
        
        if launchOptions?[UIApplicationLaunchOptionsLocationKey] != nil {
            sharedLocation.startMonitoringLocation()
        }
        
        // To handle the notification when the application is closed completely.
        if launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] != nil {
            let userInfo = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as! [NSObject : AnyObject]
            
            // Use delay execuation to solve the problem of preseting view when the App is still in launching state.
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            
            dispatch_after(delayTime, dispatch_get_main_queue(), {
                self.remoteNotificationHandler(userInfo)
            })
        }
        
        // To handle the notification when the application is closed completely.
        if launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] != nil {
            let localNotification = launchOptions?[UIApplicationLaunchOptionsLocalNotificationKey] as! UILocalNotification
            let userInfo = localNotification.userInfo
            
            // Use delay execuation to solve the problem of preseting view when the App is still in launching state.
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            
            dispatch_after(delayTime, dispatch_get_main_queue(), {
                self.notificationHandler(userInfo!, isLaunching: true)
            })
        }
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        let sharedAuth = Auth.sharedAuth
        
        if sharedAuth.debug == false {
            sharedAuth.setApnsToken(deviceTokenString)
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        // Do something if necessary
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        remoteNotificationHandler(userInfo)
    }
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        
        let userInfo = notification.userInfo!
        notificationHandler(userInfo, isLaunching: false)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        sharedLocation.restartMonitoringLocation()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        sharedLocation.startMonitoringLocation()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let filterSettings = FilterSettings.sharedFilterSettings
        filterSettings.resetSettings()
    }
    
    func notificationHandler(userInfo: [NSObject : AnyObject], isLaunching: Bool) {
        let state = UIApplication.sharedApplication().applicationState
        
        let type = userInfo["type"] as! Int
        let poiId = userInfo["poi_id"] as! Int
        
        /// Notification Types
        ///
        /// types:
        ///     1 = System
        ///     2 = POI Nearby
        ///     3 = Promotions
        if type == 2 {
            
            if state == UIApplicationState.Background || state == UIApplicationState.Inactive || isLaunching == true {
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let poiDetailVC = mainStoryboard.instantiateViewControllerWithIdentifier("PoiDetailViewController") as! PoiDetailViewController
                
                let sharedAuth = Auth.sharedAuth
                let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
                
                let sharedApi = ApiList.sharedApi
                let apiUrl = sharedApi.getPoiDetailApi(poiId)
                
                let response = Just.get(apiUrl, headers: ["Authorization": authToken])
                
                if response.ok {
                    let poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
                    poiDetailVC.poiDetailInfo = poiDetailInfo
                    poiDetailVC.poiId = poiId
                    poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
                    poiDetailVC.isPushed = false
                } else if response.statusCode == nil {
                    let alert = Alert()
                    alert.noConnectionAlert()
                } else {
                    let alert = Alert()
                    alert.unknownServerErrorAlert()
                }
                
                let navVC = UINavigationController(rootViewController: poiDetailVC)
                
                // To solve the issue of ovelapping presented view controllers
                if self.window?.rootViewController?.presentedViewController != nil {
                    self.window?.rootViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
                }
                
                self.window?.rootViewController?.presentViewController(navVC, animated: true, completion: nil)
            }
        }
    }
    
    func remoteNotificationHandler(userInfo: [NSObject : AnyObject]) {
        let type = userInfo["type"] as! Int
        
        /// Notification Types
        ///
        /// types:
        ///     1 = System
        ///     2 = POI Nearby
        ///     3 = Promotions
        ///     5 = Ads
        if type == 1 {
            let poiId = userInfo["uid"] as! Int
            
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let poiDetailVC = mainStoryboard.instantiateViewControllerWithIdentifier("PoiDetailViewController") as! PoiDetailViewController
            
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getPoiDetailApi(poiId)
            
            let response = Just.get(apiUrl, headers: ["Authorization": authToken])
            
            if response.ok {
                let poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
                poiDetailVC.poiDetailInfo = poiDetailInfo
                poiDetailVC.poiId = poiId
                poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
                poiDetailVC.isPushed = false
            } else if response.statusCode == nil {
                let alert = Alert()
                alert.noConnectionAlert()
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
            }
            
            let navVC = UINavigationController(rootViewController: poiDetailVC)
            
            // To solve the issue of ovelapping presented view controllers
            if self.window?.rootViewController?.presentedViewController != nil {
                self.window?.rootViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
            }
            
            self.window?.rootViewController?.presentViewController(navVC, animated: true, completion: nil)
        }
        
        if type == 5 {
            let uid = userInfo["uid"] as! Int
            
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getAdsApi(uid)
            
            let response = Just.get(apiUrl)
            
            if response.ok {
                let data = response.json?.objectForKey("data") as! NSDictionary
                
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let pushAdsVC = mainStoryboard.instantiateViewControllerWithIdentifier("PushAdsVC") as! PushAdsViewController
                pushAdsVC.adsData = data
                
                // To solve the issue of ovelapping presented view controllers
                if self.window?.rootViewController?.presentedViewController != nil {
                    self.window?.rootViewController?.presentedViewController?.dismissViewControllerAnimated(false, completion: nil)
                }
                
                self.window?.rootViewController?.presentViewController(pushAdsVC, animated: true, completion: nil)
            }
        }
    }
}

