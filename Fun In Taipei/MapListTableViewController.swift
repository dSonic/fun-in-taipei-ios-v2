//
//  MapListTableViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 11/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import SQLite
import Just

class MapListTableViewController: UITableViewController {
    var poiList: [PoiMarker] = []
    var tappedMarker: PoiMarker! = nil
    var poiDetailInfo: NSDictionary! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup navbar title image
        let titleImage = UIImage(named: "main_navbar_title.png")!
        self.navigationItem.titleView = UIImageView(image: titleImage)
        
        let addImage: UIImage? = UIImage(named: "map_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "backButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func backButtonPressed(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.poiList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MapListCell") as! MapListTableViewCell
        
        cell.subView.layer.borderWidth = 1.0
        
        let poiName = self.poiList[indexPath.row].poi["name"]
        let movieName = self.poiList[indexPath.row].poi["movie_name"]
        let poiAddress = self.poiList[indexPath.row].poi["address"]
        let poiThumb = self.poiList[indexPath.row].thumbnail
        let poiTypeIcon = self.poiList[indexPath.row].icon
        
        cell.poiName.text = poiName as? String
        cell.movieName.text = movieName as? String
        cell.poiAddress.text = poiAddress as? String
        cell.poiThumb.image = poiThumb
        cell.poiTypeIcon.image = poiTypeIcon
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
        
        poiDetailVC.poiDetailInfo = poiDetailInfo
        poiDetailVC.tappedMarker = tappedMarker
        poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
        poiDetailVC.poiId = tappedMarker.poi["id"] as! Int
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        let sharedAuth = Auth.sharedAuth
        let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
        let selectedIndex = tableView.indexPathForSelectedRow?.row
        let poiId = poiList[selectedIndex!].poi["id"] as! Int
        
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getPoiDetailApi(poiId)
        
        let response = Just.get(apiUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
            tappedMarker = poiList[selectedIndex!]
            
            return true
        } else {
            return false
        }
    }
    
}
