//
//  PoiInfoViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PoiInfoViewController: UIViewController, PoiInfoContentDelegate {

    @IBOutlet weak var contentVC: PoiInfoContentViewController!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tmpLabel: UILabel!
    
    var poiDetailInfo: NSDictionary!
    var tappedMarker: PoiMarker!
    var poiId: Int!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        //let size = tmpLabel.sizeThatFits(tmpLabel.frame.size)
        //scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, 800.0)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiInfoSegue" {
            self.contentVC = segue.destinationViewController as! PoiInfoContentViewController
            self.contentVC.poiDetailInfo = self.poiDetailInfo
            self.contentVC.delegate = self
            contentVC.tappedMarker = tappedMarker
            contentVC.poiId = poiId
        }
    }
    
    func resizeContentSize(contentHeight: CGFloat) {
        scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width, contentHeight)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
