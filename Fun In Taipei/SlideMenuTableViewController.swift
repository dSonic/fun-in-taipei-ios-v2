//
//  SlideMenuTableViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/10/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class SlideMenuTableViewController: UITableViewController {
    private var menuItems: Array<String> = []
    private var authToken: String = ""
    private var userName: String!
    private var myLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let sharedAuth = Auth.sharedAuth
        authToken = sharedAuth.authToken
        
        if authToken.isEmpty == true {
            menuItems = ["TopProfileCell", "ProfileLabelCell", "SignInCell",
                "AboutLabelCell", "ContactUsCell", "AboutUsCell"]
            userName = "未登入"
        } else {
            menuItems = ["TopProfileCell", "ProfileLabelCell", "ProfileEditingCell",
                "SignOutCell", "AboutLabelCell", "ContactUsCell", "AboutUsCell"]
            userName = sharedAuth.userName
        }
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "slide_menu_bg"))
    }
    
    override func viewWillDisappear(animated: Bool) {
        // Solve the problem of stacking labels for Static Table Cells
        myLabel.removeFromSuperview()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menuItems.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let CellIdentifier = menuItems[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath)
        
        if (CellIdentifier == "ProfileLabelCell" || CellIdentifier == "AboutLabelCell") {
            cell.backgroundView = UIImageView(image: UIImage(named: "slide_menu_title_bg"))
        }
        
        if (CellIdentifier == "TopProfileCell") {
            myLabel = UILabel(frame: CGRect(x: 70, y: 8, width: 242, height: 54))
            myLabel.textColor = UIColor.whiteColor()
            myLabel.text = userName
            cell.addSubview(myLabel)
            cell.backgroundView = UIImageView(image: UIImage(named: "slide_menu_title_bg"))
        }
        
        if (CellIdentifier == "ProfileEditingCell" || CellIdentifier == "SignOutCell" ||
            CellIdentifier == "ContactUsCell" || CellIdentifier == "AboutUsCell" ||
            CellIdentifier == "SignInCell") {
            cell.backgroundView = UIImageView(image: UIImage(named: "slide_menu_cell_bg"))
        }
        
        cell.selectedBackgroundView = UIImageView(image: UIImage(named: "slide_menu_cell_over_bg"))

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let CellIdentifier = menuItems[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifier, forIndexPath: indexPath)
        cell.backgroundView = UIImageView(image: UIImage(named: "slide_menu_cell_over_bg"))
        
        let revealController = self.revealViewController()
        
        if menuItems[indexPath.row] == "SignOutCell" {
            let sharedAuth = Auth.sharedAuth
            sharedAuth.signOut()
            
            // closing slide out menu
            revealController.revealToggleAnimated(true)
            
            // switch view to Map View after signing out
            let tabVC = revealController.frontViewController as! UITabBarController
            tabVC.selectedIndex = 0
        }
        
        if menuItems[indexPath.row] == "SignInCell" {
            revealController.revealToggleAnimated(true)
            
            let authRootVC = self.storyboard?.instantiateViewControllerWithIdentifier("AuthRootViewController") as! AuthRootViewController
            self.presentViewController(authRootVC, animated: true, completion: nil)
        }
        
        if menuItems[indexPath.row] == "ProfileEditingCell" {
            revealController.revealToggleAnimated(true)
            
            let profileEditorVC = self.storyboard?.instantiateViewControllerWithIdentifier("ProfileEditorNavController") as! ProfileEditorNavController
            self.presentViewController(profileEditorVC, animated: true, completion: nil)
        }
        
        if menuItems[indexPath.row] == "ContactUsCell" {
            revealController.revealToggleAnimated(true)
            
            let feedbackVC = self.storyboard?.instantiateViewControllerWithIdentifier("FeedbackNavViewController") as! FeedbackNavViewController
            self.presentViewController(feedbackVC, animated: true, completion: nil)
        }
        
        if menuItems[indexPath.row] == "AboutUsCell" {
            revealController.revealToggleAnimated(true)
            
            let appLink = "itms-apps://itunes.apple.com/tw/app/fang-ying-tai-bei/id967846978?mt=8"
            UIApplication.sharedApplication().openURL(NSURL(string: appLink)!)
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let CellIdentifier = menuItems[indexPath.row]
        
        if CellIdentifier == "ProfileLabelCell" || CellIdentifier == "AboutLabelCell" {
            return 30.0
        }
        
        return 70.0
    }

}
