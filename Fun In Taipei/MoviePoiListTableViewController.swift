//
//  MoviePoiListTableViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/13/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class MoviePoiListTableViewController: UITableViewController {
    var poiList: Array<NSDictionary> = []
    var typeImages: Array<String> = ["", "pin_food", "pin_store", "pin_attraction"]
    
    var poiDetailInfo: NSDictionary!
    var selectedPoiId: Int!
    var selectedLatitude: String!
    var selectedLongitude: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "景點列表"
        
        drawBackButton()
    }
    
    private func drawBackButton() {
        let btnImage = UIImage(named: "nav_back_btn")!
        let backButton = UIButton(type: .Custom)
        
        backButton.frame = CGRectMake(0, 0, btnImage.size.width, btnImage.size.height)
        backButton.setBackgroundImage(btnImage, forState: .Normal)
        backButton.addTarget(self, action: "backButtonPressed:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: backButton), animated: false)
    }
    
    func backButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return poiList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MoviePoiListCell", forIndexPath: indexPath) as! MoviePoiListTableViewCell
        
        let imageUrl = poiList[indexPath.row].objectForKey("thumbnail") as! String
        let typeId = poiList[indexPath.row].objectForKey("type") as! Int
        let typeImageString = typeImages[typeId]
        
        cell.borderView.layer.borderColor = UIColor.blackColor().CGColor
        cell.borderView.layer.borderWidth = 1.0
        cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: imageUrl)!)
        cell.poiNameLabel.text = poiList[indexPath.row].objectForKey("name") as? String
        cell.poiMovieLabel.text = poiList[indexPath.row].objectForKey("movie_name") as? String
        cell.poiAddressLabel.text = poiList[indexPath.row].objectForKey("address") as? String
        cell.poiTypeImageView.image = UIImage(named: typeImageString)

        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90.0
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // do something
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "MoviePoiToDetailSegue" {
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            let selectedIndex = tableView.indexPathForSelectedRow?.row
            let poiId = poiList[selectedIndex!].objectForKey("id") as! Int
            
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getPoiDetailApi(poiId)
            
            let response = Just.get(apiUrl, headers: ["Authorization": authToken])
            
            if response.ok {
                poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
                
                return true
            } else if response.statusCode == 401 {
                let alert = Alert()
                alert.unknownServerErrorAlert()
                
                return false
            } else {
                let alert = Alert()
                alert.noConnectionAlert()
                
                return false
            }
        }
        
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
        
        let selectedIndex = tableView.indexPathForSelectedRow!.row
        
        poiDetailVC.poiId = poiList[selectedIndex].objectForKey("id") as! Int
        poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
        poiDetailVC.poiDetailInfo = poiDetailInfo
    }

    
}
