//
//  SigninContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class SigninContentViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.accountTextField.becomeFirstResponder()
        
        let sharedAuth = Auth.sharedAuth
        
        if sharedAuth.debug == true {
            accountTextField.text = "dsonic@gmail.com"
            passwordTextField.text = "asdfghjk"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.passwordTextField {
            self.SignInUser()
        }
        
        if textField == self.accountTextField {
            self.accountTextField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        }
        
        return true
    }

    /**
     * Limit the number of characters can be typed into the text fields.
     */
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == self.accountTextField {
            let characterCount = textField.text?.characters.count ?? 0
            
            if (range.length + range.location > characterCount) {
                return false
            }
            
            let newLength = characterCount + string.characters.count - range.length
            return newLength <= 50
        } else if textField == self.passwordTextField {
            let characterCount = textField.text?.characters.count ?? 0
            
            if (range.length + range.location > characterCount) {
                return false
            }
            
            let newLength = characterCount + string.characters.count - range.length
            return newLength <= 12
        }
        
        return true
    }
    
    /**
     * Sign in User method
     */
    
    private func SignInUser() {
        let sharedAuth = Auth.sharedAuth
        let username = self.accountTextField!.text
        let password = self.passwordTextField!.text
        
        let signin = sharedAuth.signIn(username!, password: password!)
        
        if signin == 200 {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else if signin == 300 {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = UIAlertView(
                title: "登入失敗",
                message: "請檢查帳號與密碼是否正確...",
                delegate: self, cancelButtonTitle: "確認")
            
            alert.show()
        }
    }
    
    @IBAction func signInButtonPressed(sender: UIButton) {
        self.SignInUser()
    }
}
