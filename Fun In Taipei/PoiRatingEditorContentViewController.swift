//
//  PoiRatingEditorContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/21/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import SwiftValidator
import RSKImageCropper
import Just

protocol PoiRatingEditorContentDelegate {
    func contentHeightOfContainer(height: CGFloat)
    func reloadRatingsTable()
}

class PoiRatingEditorContentViewController: UIViewController, ValidationDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var nickNameTextField: UITextField!
    @IBOutlet weak var commentText: UITextView!
    
    @IBOutlet weak var happyButton: UIButton!
    @IBOutlet weak var funnyButton: UIButton!
    @IBOutlet weak var surpriseButton: UIButton!
    @IBOutlet weak var coolButton: UIButton!
    @IBOutlet weak var satisfyButton: UIButton!
    @IBOutlet weak var normalButton: UIButton!
    @IBOutlet weak var boringButton: UIButton!
    @IBOutlet weak var upsetButton: UIButton!
    @IBOutlet weak var cryButton: UIButton!
    @IBOutlet weak var angryButton: UIButton!
    
    @IBOutlet weak var nickNameErrorLabel: UILabel!
    @IBOutlet weak var moodErrorLabel: UILabel!
    @IBOutlet weak var commentErrorLabel: UILabel!
    
    private var buttonImages = [
        UIImage(named: "rating_mood_happy_btn"),
        UIImage(named: "rating_mood_happy_btn"),
        UIImage(named: "rating_mood_fun_btn"),
        UIImage(named: "rating_mood_surprise_btn"),
        UIImage(named: "rating_mood_cool_btn"),
        UIImage(named: "rating_mood_satisfy_btn"),
        UIImage(named: "rating_mood_normal_btn"),
        UIImage(named: "rating_mood_boring_btn"),
        UIImage(named: "rating_mood_upset_btn"),
        UIImage(named: "rating_mood_cry_btn"),
        UIImage(named: "rating_mood_angry_btn")
    ]
    
    private var buttonImagesPressed = [
        UIImage(named: "rating_mood_happy_btn_selected"),
        UIImage(named: "rating_mood_happy_btn_selected"),
        UIImage(named: "rating_mood_fun_btn_selected"),
        UIImage(named: "rating_mood_surprise_btn_selected"),
        UIImage(named: "rating_mood_cool_btn_selected"),
        UIImage(named: "rating_mood_satisfy_btn_selected"),
        UIImage(named: "rating_mood_normal_btn_selected"),
        UIImage(named: "rating_mood_boring_btn_selected"),
        UIImage(named: "rating_mood_upset_btn_selected"),
        UIImage(named: "rating_mood_cry_btn_selected"),
        UIImage(named: "rating_mood_angry_btn_selected")
    ]
    
    var delegate: PoiRatingEditorContentDelegate! = nil
    let validator = Validator()
    private var mood = 1
    var poiId = 0
    var uploadImage: UIImage! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        validator.registerField(nickNameTextField, errorLabel: nickNameErrorLabel,
            rules: [RequiredRule(message: "請輸入您的暱稱")])
        happyButton.setBackgroundImage(buttonImagesPressed[1], forState: .Normal)
        
        let uploadImageViewRecognizer = UITapGestureRecognizer(target: self, action: Selector("handleUploadRecognizer:"))
        uploadImageView.addGestureRecognizer(uploadImageViewRecognizer)
        
        commentText.layer.borderColor = UIColor.grayColor().CGColor
        commentText.layer.borderWidth = 1.0
        commentText.layer.cornerRadius = 12.0
        commentText.layer.masksToBounds = false
        commentText.layer.shouldRasterize = true
        commentText.clipsToBounds = true // to solve the text displayed out of bounds
        
        nickNameTextField.layer.borderColor = UIColor.grayColor().CGColor
        nickNameTextField.layer.borderWidth = 1.0
        nickNameTextField.layer.masksToBounds = false
        nickNameTextField.layer.shouldRasterize = true
    }
    
    override func viewDidAppear(animated: Bool) {
        nickNameTextField.becomeFirstResponder()
    }

    override func viewDidLayoutSubviews() {
        let yOrigin = uploadImageView.frame.origin.y
        let elementHeight = uploadImageView.frame.size.height
        let contentHeight = yOrigin + elementHeight + 50
        
        delegate.contentHeightOfContainer(contentHeight)
    }
    
    private func clearButtonStates() {
        for i in (1...10) {
            let button = self.view.viewWithTag(i) as! UIButton
            button.setBackgroundImage(buttonImages[i], forState: .Normal)
        }
    }
    
    @IBAction func moodButtonPressed(sender: UIButton) {
        let buttonTag = sender.tag
        
        clearButtonStates()
        sender.setBackgroundImage(buttonImagesPressed[buttonTag], forState: .Normal)
        
        mood = buttonTag
    }
    
    func confirmButtonPressed() {
        nickNameErrorLabel.hidden = true
        nickNameTextField.layer.borderColor = UIColor.blackColor().CGColor
        nickNameTextField.layer.borderWidth = 0.1
        
        validator.validate(self)
    }
    
    func validationSuccessful() {
        if commentText.text.isEmpty == true {
            commentErrorLabel.text = "您還未寫評論喔！"
            commentErrorLabel.hidden = false
        } else {
            commentErrorLabel.hidden = true
            
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            let apiUrl = ApiList.sharedApi.getRatingEditingApi(poiId)
            
            let params: [String : AnyObject] = [
                "nickname": nickNameTextField.text!,
                "mood": mood,
                "message": commentText.text!
            ]
            
            var response: HTTPResult?
            
            // Check if there is an image to upload
            if uploadImage == nil {
                response = Just.post(apiUrl,
                    headers: ["Authorization": authToken],
                    data: params
                )
            } else {
                let imageData = UIImagePNGRepresentation(uploadImage)
                
                response = Just.post(apiUrl,
                    headers: ["Authorization": authToken],
                    data: params,
                    files: [
                        "image": .Data("poi_image.png", imageData!, nil)
                    ]
                )
            }
            
            if (response?.ok != nil) {
                delegate.reloadRatingsTable()
                
                let status = response?.json?.objectForKey("status") as! Int
                
                if status == 500 {
                    let alert = UIAlertView(title: "溫馨提醒", message: "不好意思，同一個景點7天內只能評論一次...",
                        delegate: nil, cancelButtonTitle: "確認")
                    alert.show()
                }
                
                self.dismissViewControllerAnimated(true, completion: nil)
            } else if response?.statusCode == nil {
                let alert = Alert()
                alert.noConnectionAlert()
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in validator.errors {
            field.layer.borderColor = UIColor.redColor().CGColor
            field.layer.borderWidth = 1.0
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.hidden = false
        }
    }
    
    func handleUploadRecognizer(sender: UITapGestureRecognizer) {
        presentViewController(generateUploadPhotoActionSheet(), animated: true, completion: nil)
    }
    
    func pickMediaFromSource(sourceType: UIImagePickerControllerSourceType) {
        let mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(sourceType)!
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType) && mediaTypes.count > 0 {
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = sourceType
            presentViewController(picker, animated: true, completion: nil)
        } else {
            let alert = UIAlertView(title: "溫馨提醒", message: "只裝置並不支援您所選的圖片來源...", delegate: self, cancelButtonTitle: "確認")
            alert.show()
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {

        uploadImage = info[UIImagePickerControllerEditedImage] as! UIImage
        uploadImageView.hnk_setImage(uploadImage!, animated: false, success: nil)
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func generateUploadPhotoActionSheet() -> UIAlertController {
        let alertController = UIAlertController(title: "選擇圖片來源",
            message: nil, preferredStyle: .ActionSheet)
        
        let cameraAction = UIAlertAction(title: "開啟相機", style: .Default, handler: {action in
            self.pickMediaFromSource(UIImagePickerControllerSourceType.Camera)
        })
        
        let photoLibraryAction = UIAlertAction(title: "開啟相簿", style: .Default, handler: {action in
            self.pickMediaFromSource(UIImagePickerControllerSourceType.PhotoLibrary)
        })
        
        let cancelAction = UIAlertAction(title: "取消", style: .Default, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == nickNameTextField {
            nickNameTextField.resignFirstResponder()
            commentText.becomeFirstResponder()
        }
        
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        guard let myText = textView.text else {return true}
        
        let newLength = myText.characters.count + text.characters.count - range.length
        return newLength <= 150
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        // to solve the text displayed out of bounds
        commentText.contentInset = UIEdgeInsetsZero
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 10
    }
}
