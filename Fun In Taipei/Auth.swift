//
//  Auth.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation
import Just

class Auth {
    class var sharedAuth: Auth {
        struct Singleton {
            static let instance = Auth()
        }
        
        return Singleton.instance
    }
    
    private(set) var authToken:String
    private(set) var apnsToken:String
    private(set) var userName:String
    private(set) var debug = false
    private let debugApnsToken = "122e5d9cf1bb442ddf193c1d1c03c70ebd4a594483f088fbe0e88e3830d431f2"
    
    init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let storedToken = defaults.objectForKey("auth_token") as? String
        let storedApnsToken = defaults.objectForKey("apns_token") as? String
        let storedUserName = defaults.objectForKey("user_name") as? String
        
        authToken = storedToken != nil ? storedToken! : ""

        if debug == true {
            apnsToken = debugApnsToken
        } else {
            apnsToken = storedApnsToken != nil ? storedApnsToken! : ""
        }
        
        userName = storedUserName != nil ? storedUserName! : ""
    }
    
    func setAuthToken(token: String) {
        self.authToken = token
        
        saveAuthToken()
    }
    
    func setApnsToken(token: String) {
        self.apnsToken = token
        
        saveApnsToken()
    }
    
    func setUserName(userName: String) {
        self.userName = userName
        saveUserName()
    }
    
    /// A method for Signing In users
    ///
    /// - parameters:
    ///   - username: A `String` of an account username
    ///   - password: A `String` of an account password
    ///
    /// - returns: statusCode: A `Int` of status code of http request response
    func signIn(username:String, password:String) -> Int{
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getApiUrl("signin")
        
        apnsToken = debug == true ? debugApnsToken : apnsToken
        
        let response = Just.post(
            apiUrl,
            data: [
                "username": username,
                "password": password,
                "uid": apnsToken,
                "token": apnsToken,
                "type": 1,
                "lang": "zh_TW"
            ]
        )
        
        if response.ok {
            let data = response.json?.objectForKey("data") as! NSDictionary
            let token = data.objectForKey("token") as! String
            let userName = data.objectForKey("name") as! String
            
            setAuthToken(token)
            setUserName(userName)

            return 200
        } else if response.statusCode == nil {
            return 300
        } else {
            return 400
        }
    }
    
    /// A method for Signin Up user
    ///
    /// - parameters:
    ///   - username: A `String` of an account username
    ///   - password: A `String` of an account password
    ///   - firstName: A `String` of a user's first name
    ///   - lastName: A `String` of a user's last name
    ///   - tel: A `String` of a user's telephone number
    ///
    /// - returns: statusCode: A `Int` of status code of http request response
    func signUp(username:String, password:String, firstName:String,
        lastName:String, tel:String) -> Int {
            
        let apiShared = ApiList.sharedApi
        let apiUrl = apiShared.getApiUrl("signup")
            
        let response = Just.post(
            apiUrl,
            data: [
                "email": username,
                "password": password,
                "first_name": firstName,
                "last_name": lastName,
                "tel": tel,
                "uid": apnsToken,
                "token": apnsToken,
                "type": 1,
                "lang": "zh-Hans"
            ]
        )
        
        if response.ok {
            let data = response.json?.objectForKey("data") as! [String : AnyObject]
            authToken = data["token"] as! String
            userName = data["name"] as! String
                
            return 200
        } else if response.statusCode == nil {
            return 300
        } else {
            let status = response.json?.objectForKey("status") as! Int
            return status
        }
    }
    
    func signOut() {
        authToken = ""
        userName = ""
        saveAuthToken()
        saveUserName()
    }
    
    private func saveAuthToken() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.authToken, forKey: "auth_token")
        defaults.synchronize()
    }
    
    private func saveApnsToken() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.apnsToken, forKey: "apns_token")
        defaults.synchronize()
    }
    
    private func saveUserName() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.userName, forKey: "user_name")
        defaults.synchronize()
    }
}
