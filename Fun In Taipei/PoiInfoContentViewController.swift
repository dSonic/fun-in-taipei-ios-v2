//
//  PoiInfoContentViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/5/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol PoiInfoContentDelegate {
    func resizeContentSize(contentHeight: CGFloat)
}

class PoiInfoContentViewController: UIViewController {
    
    @IBOutlet weak var poiImageView: UIImageView!
    @IBOutlet weak var poiImageView2: UIImageView!
    @IBOutlet weak var poiImageView3: UIImageView!
    @IBOutlet weak var poiThumbImg1: UIImageView!
    @IBOutlet weak var poiThumbImg2: UIImageView!
    @IBOutlet weak var poiThumbImg3: UIImageView!
    
    @IBOutlet weak var poiTypeLabel: UILabel!
    @IBOutlet weak var poiTitleLabel: UILabel!
    @IBOutlet weak var poiPhoneLabel: UILabel!
    @IBOutlet weak var poiWebsiteLabel: UILabel!
    @IBOutlet weak var poiFacebookLabel: UILabel!
    @IBOutlet weak var poiAddressLabel: UILabel!
    @IBOutlet weak var poiBusinessHourLabel: UILabel!
    @IBOutlet weak var poiTranportationLabel: UILabel!
    @IBOutlet weak var poiDirectionLabel: UILabel!
    
    var poiDetailInfo: NSDictionary!
    var poiImages: Array<NSDictionary>!
    var tappedMarker: PoiMarker!
    var poiId: Int!
    
    var delegate: PoiInfoContentDelegate! = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        poiImages = (poiDetailInfo.objectForKey("images") as! NSArray) as! Array
        
        var count = 0
        
        for poiImage in poiImages {
            let imgUrl = poiImage.objectForKey("image") as! String
            let URL = NSURL(string: imgUrl)!
            
            if count == 0 {
                poiImageView.hnk_setImageFromURL(URL)
                poiThumbImg1.hnk_setImageFromURL(URL)
                
                poiThumbImg1.layer.borderWidth = 2.0
                poiThumbImg1.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            if count == 1 {
                poiImageView2.hnk_setImageFromURL(URL)
                poiThumbImg2.hnk_setImageFromURL(URL)
                
                poiThumbImg2.layer.borderWidth = 2.0
                poiThumbImg2.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            if count == 2 {
                poiImageView3.hnk_setImageFromURL(URL)
                poiThumbImg3.hnk_setImageFromURL(URL)
                
                poiThumbImg3.layer.borderWidth = 2.0
                poiThumbImg3.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            count = count+1
        }
        
        let transportation = poiDetailInfo.objectForKey("transportation")! as! String
        let address = poiDetailInfo.objectForKey("address")! as! String
        let phone = poiDetailInfo.objectForKey("tel")! as! String
        let website = poiDetailInfo.objectForKey("website")! as! String
        let facebook = poiDetailInfo.objectForKey("facebook")! as! String
        
        poiTitleLabel.text = String(poiDetailInfo.objectForKey("name")!)
        poiBusinessHourLabel.text = String(poiDetailInfo.objectForKey("business_hour")!)
        poiPhoneLabel.text = phone.isEmpty == false ? phone : "無資料"
        poiAddressLabel.text = address.isEmpty == false ? address : "無資料"
        poiTranportationLabel.text = transportation.isEmpty == false ? transportation : "無資料"
        poiWebsiteLabel.text = website.isEmpty == false ? website : "無資料"
        poiFacebookLabel.text = facebook.isEmpty == false ? facebook : "無資料"
        
        if website.isEmpty != true {
            let websiteLabelRecognizer = UITapGestureRecognizer(target: self,
                action: Selector("handleWebsiteLabelRecognizer:"))
            poiWebsiteLabel.addGestureRecognizer(websiteLabelRecognizer)
        }
        
        if facebook.isEmpty != true {
            let facebookLabelRecognizer = UITapGestureRecognizer(target: self,
                action: Selector("handleFacebookLabelRecognizer:"))
            poiFacebookLabel.addGestureRecognizer(facebookLabelRecognizer)
        }
        
        if phone.isEmpty != true {
            let phoneLabelRecognizer = UITapGestureRecognizer(target: self,
                action: Selector("handlePhoneLabelRecognizer:"))
            poiPhoneLabel.addGestureRecognizer(phoneLabelRecognizer)
        }
        
        let directionLabelRecognizer = UITapGestureRecognizer(target: self,
            action: Selector("handleDirectionRecognizer:"))
        poiDirectionLabel.addGestureRecognizer(directionLabelRecognizer)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let thumbImgTapRecognizer = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer:"))
        let thumbImgTapRecognizer2 = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer2:"))
        let thumbImgTapRecognizer3 = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer3:"))
        
        poiThumbImg1.addGestureRecognizer(thumbImgTapRecognizer)
        poiThumbImg2.addGestureRecognizer(thumbImgTapRecognizer2)
        poiThumbImg3.addGestureRecognizer(thumbImgTapRecognizer3)
    }
    
    override func viewDidLayoutSubviews() {
        let tranSize = poiTranportationLabel.sizeThatFits(poiTranportationLabel.frame.size)
        let addressSize = poiAddressLabel.sizeThatFits(poiAddressLabel.frame.size)
        let businessHourSize = poiBusinessHourLabel.sizeThatFits(poiBusinessHourLabel.frame.size)
        
        poiTranportationLabel.frame.size.height = tranSize.height
        poiAddressLabel.frame.size.height = addressSize.height
        poiBusinessHourLabel.frame.size.height = businessHourSize.height
        
        let contentHeight = poiFacebookLabel.frame.origin.y + 50.0
        delegate.resizeContentSize(contentHeight)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleThumbImgTapRecognizer(sender: UITapGestureRecognizer) {
        poiImageView2.hidden = true
        poiImageView3.hidden = true
        
        poiImageView.hidden = false
    }
    
    func handleThumbImgTapRecognizer2(sender: UITapGestureRecognizer) {
        poiImageView.hidden = true
        poiImageView3.hidden = true
        
        poiImageView2.hidden = false
    }
    
    func handleThumbImgTapRecognizer3(sender: UITapGestureRecognizer) {
        poiImageView.hidden = true
        poiImageView2.hidden = true
        
        poiImageView3.hidden = false
    }
    
    func handleWebsiteLabelRecognizer(sender: UITapGestureRecognizer) {
        let websiteLabel = sender.view as! UILabel
        let url = websiteLabel.text!
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
    func handleFacebookLabelRecognizer(sender: UITapGestureRecognizer) {
        let facebookLabel = sender.view as! UILabel
        let url = facebookLabel.text!
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }
    
    func handleDirectionRecognizer(sender: UITapGestureRecognizer) {
        let myLatitude = poiDetailInfo.objectForKey("latitude") as! String
        let myLongitude = poiDetailInfo.objectForKey("longitude") as! String
        
        UIApplication.sharedApplication().openURL(
        NSURL(string: "https://www.google.com.tw/maps/dir//\(myLatitude),\(myLongitude)/")!)
    }
    
    func handlePhoneLabelRecognizer(sender: UITapGestureRecognizer) {
        let phoneLabel = sender.view as! UILabel
        let phoneNumber = phoneLabel.text!
        
        UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(phoneNumber)")!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
