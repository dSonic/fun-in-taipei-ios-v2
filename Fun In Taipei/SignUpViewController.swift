//
//  SignUpViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/18/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol SignUpViewDelegate {
    func switchFromSignUpToSignIn()
    func switchFromSignUpToForgetPass()
}

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var signInTab: UIButton!
    @IBOutlet weak var forgetPassTab: UIButton!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var signUpTab: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    var delegate: SignUpViewDelegate! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        let lineView = UIView(frame: CGRectMake(self.signInTab.frame.size.width, 0, 1, self.signInTab.frame.size.height))
        lineView.backgroundColor = UIColor.blackColor()
        
        let lineView2 = UIView(frame: CGRectMake(self.signUpTab.frame.size.width, 0, 1, self.signUpTab.frame.size.height))
        lineView2.backgroundColor = UIColor.blackColor()
        
        self.signInTab.addSubview(lineView)
        self.signUpTab.addSubview(lineView2)
        
        let label: UILabel = UILabel()
        label.font = UIFont(name: "DFWZMing-W4-WIN-BF", size: 24)
        label.textColor = UIColor.whiteColor()
        label.text = "登入/註冊"
        label.sizeToFit()
        
        self.navigationItem.titleView = label
        
        drawCancelButton()
        
        self.navBar.pushNavigationItem(self.navigationItem, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        self.scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, 600.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func signInButtonPressed(sender: UIButton) {
        delegate.switchFromSignUpToSignIn()
    }

    @IBAction func forgetPassButtonPressed(sender: UIButton) {
        delegate.switchFromSignUpToForgetPass()
    }

}
