//
//  PoiPromotionScrollViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/8/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PoiPromotionScrollViewController: UIViewController {
    
    var delegate: PoiPromotionDelegate! = nil // for passing delegate to the Promotion Content View.
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiPromotionContentSegue" {
            let poiPromotionContentVC = segue.destinationViewController as! PoiPromotionContentViewController
            poiPromotionContentVC.delegate = delegate
        }
    }
    
}
