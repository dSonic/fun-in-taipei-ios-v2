//
//  MapListTableViewCell.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 11/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class MapListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var poiName: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var poiAddress: UILabel!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var poiThumb: UIImageView!
    @IBOutlet weak var poiTypeIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
