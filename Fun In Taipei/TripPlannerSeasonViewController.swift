//
//  TripPlannerSeasonViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol TripPlannerSeasonViewDelegate {
    func moveFromSeasonToDays()
    func confirmButtonToggleFromSeason()
}

class TripPlannerSeasonViewController: UIViewController {
    @IBOutlet weak var springButton: UIButton!
    @IBOutlet weak var summerButton: UIButton!
    @IBOutlet weak var fallButton: UIButton!
    @IBOutlet weak var winterButton: UIButton!
    
    private var buttonImages: Array<UIImage> = [
        UIImage(named: "trip_planner_spring_btn")!,
        UIImage(named: "trip_planner_spring_btn")!,
        UIImage(named: "trip_planner_summer_btn")!,
        UIImage(named: "trip_planner_fall_btn")!,
        UIImage(named: "trip_planner_winter_btn")!
    ]
    
    private var buttonPressedImages: Array<UIImage> = [
        UIImage(named: "trip_planner_spring_btn_pressed")!,
        UIImage(named: "trip_planner_spring_btn_pressed")!,
        UIImage(named: "trip_planner_summer_btn_pressed")!,
        UIImage(named: "trip_planner_fall_btn_pressed")!,
        UIImage(named: "trip_planner_winter_btn_pressed")!,
    ]
    
    private let sharedSettings = TripPlannerSettings.sharedTripPlannerSettings
    private var settings: Dictionary<String,Int>!
    
    var delegate: TripPlannerSeasonViewDelegate! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settings = sharedSettings.settings as! Dictionary
        
        let imageIndex = settings["season"]!
        
        let buttonImage = buttonPressedImages[imageIndex]
        var selectedButton: UIButton!
        
        switch imageIndex {
            case 1:
                selectedButton = springButton
            case 2:
                selectedButton = summerButton
            case 3:
                selectedButton = fallButton
            case 4:
                selectedButton = winterButton
            default:
                selectedButton = nil
        }
        
        if selectedButton != nil {
            selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
        }
    }

    private func switchToButton(sender: UIButton) {
        let buttonImage = buttonPressedImages[sender.tag]
        var selectedButton: UIButton!
        
        switch sender.tag {
            case 1:
                selectedButton = springButton
                summerButton.setBackgroundImage(buttonImages[2], forState: .Normal)
                fallButton.setBackgroundImage(buttonImages[3], forState: .Normal)
                winterButton.setBackgroundImage(buttonImages[4], forState: .Normal)
            case 2:
                selectedButton = summerButton
                springButton.setBackgroundImage(buttonImages[1], forState: .Normal)
                fallButton.setBackgroundImage(buttonImages[3], forState: .Normal)
                winterButton.setBackgroundImage(buttonImages[4], forState: .Normal)
            case 3:
                selectedButton = fallButton
                springButton.setBackgroundImage(buttonImages[1], forState: .Normal)
                summerButton.setBackgroundImage(buttonImages[2], forState: .Normal)
                winterButton.setBackgroundImage(buttonImages[4], forState: .Normal)
            case 4:
                selectedButton = winterButton
                springButton.setBackgroundImage(buttonImages[1], forState: .Normal)
                summerButton.setBackgroundImage(buttonImages[2], forState: .Normal)
                fallButton.setBackgroundImage(buttonImages[3], forState: .Normal)
            default:
                selectedButton = springButton
                summerButton.setBackgroundImage(buttonImages[2], forState: .Normal)
                fallButton.setBackgroundImage(buttonImages[3], forState: .Normal)
                winterButton.setBackgroundImage(buttonImages[4], forState: .Normal)
        }
        
        selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
    }
    
    @IBAction func springButtonPressed(sender: UIButton) {
        self.switchToButton(sender)
        settings["season"] = 1
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.moveFromSeasonToDays()
        delegate.confirmButtonToggleFromSeason()
    }
    
    @IBAction func summerButtonPressed(sender: UIButton) {
        self.switchToButton(sender)
        settings["season"] = 2
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.moveFromSeasonToDays()
        delegate.confirmButtonToggleFromSeason()
    }
    
    @IBAction func fallButtonPressed(sender: UIButton) {
        self.switchToButton(sender)
        settings["season"] = 3
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.moveFromSeasonToDays()
        delegate.confirmButtonToggleFromSeason()
    }
    
    @IBAction func winterButtonPressed(sender: UIButton) {
        self.switchToButton(sender)
        settings["season"] = 4
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.moveFromSeasonToDays()
        delegate.confirmButtonToggleFromSeason()
    }
    
}
