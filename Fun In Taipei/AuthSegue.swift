//
//  AuthSegue.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 11/24/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

@objc(AuthSegue)
class AuthSegue: UIStoryboardSegue {
    override func perform() {
        let src = self.sourceViewController as UIViewController
        let dst = self.destinationViewController as UIViewController
        src.navigationController?.pushViewController(dst, animated: false)
    }
}
