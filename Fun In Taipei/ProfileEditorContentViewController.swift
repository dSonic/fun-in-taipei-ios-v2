//
//  ProfileEditorContentViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/27/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import SwiftValidator
import Just

protocol ProfileEditorContentDelegate {
    func contentHeight(height: CGFloat)
}

class ProfileEditorContentViewController: UIViewController, ValidationDelegate, UITextFieldDelegate {

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var currentPassTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var currentPassErrorLabel: UILabel!
    @IBOutlet weak var newPassErrorLabel: UILabel!
    @IBOutlet weak var confirmPassErrorLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var delegate: ProfileEditorContentDelegate! = nil
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        let apiUrl = ApiList.sharedApi.getApiUrl("user_profile")
        
        let response = Just.get(apiUrl,
            headers: ["Authorization": authToken]
        )
        
        if response.ok {
            let jsonData = response.json as! NSDictionary
            let data = jsonData.objectForKey("data") as! [String : AnyObject]
            
            let lastName = data["last_name"] as! String
            let firstName = data["first_name"] as! String
            let email = data["email"] as! String
            let tel = data["tel"] as! String
            
            nameLabel.text = "\(lastName) \(firstName)"
            emailLabel.text = email
            phoneLabel.text = tel
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
        
        
        validator.registerField(currentPassTextField, errorLabel: currentPassErrorLabel,
            rules: [RequiredRule(message: "請輸入密碼")])
        validator.registerField(newPassTextField, errorLabel: newPassErrorLabel,
            rules: [RequiredRule(message: "請輸入密碼"), MaxLengthRule(length: 12, message: "密碼長度不能大於12"),
                MinLengthRule(length: 8, message: "密碼長度不能小於8")])
        validator.registerField(confirmPassTextField, errorLabel: confirmPassErrorLabel, rules: [ConfirmationRule(confirmField: newPassTextField, message: "密碼不符合")])
    }
    
    override func viewDidLayoutSubviews() {
        let yPos = saveButton.frame.origin.y
        let buttonHeight = saveButton.frame.size.height
        let contentHeight = yPos + buttonHeight + 30
        
        delegate.contentHeight(contentHeight)
    }
    
    override func viewDidAppear(animated: Bool) {
        currentPassTextField.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == currentPassTextField {
            currentPassTextField.resignFirstResponder()
            newPassTextField.becomeFirstResponder()
        }
        
        if textField == newPassTextField {
            newPassTextField.resignFirstResponder()
            confirmPassTextField.becomeFirstResponder()
        }
        
        if textField == confirmPassTextField {
            saveButtonPressed(saveButton)
        }
        
        return true
    }
    
    @IBAction func saveButtonPressed(sender: UIButton) {
        currentPassTextField.layer.borderColor = UIColor.blackColor().CGColor
        currentPassTextField.layer.borderWidth = 0.1
        currentPassErrorLabel.hidden = true
        newPassTextField.layer.borderColor = UIColor.blackColor().CGColor
        newPassTextField.layer.borderWidth = 0.1
        newPassErrorLabel.hidden = true
        confirmPassTextField.layer.borderColor = UIColor.blackColor().CGColor
        confirmPassTextField.layer.borderWidth = 0.1
        confirmPassErrorLabel.hidden = true
        
        validator.validate(self)
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in validator.errors {
            field.layer.borderColor = UIColor.redColor().CGColor
            field.layer.borderWidth = 1.0
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.hidden = false
        }
    }
    
    func validationSuccessful() {
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        
        let apiUrl = ApiList.sharedApi.getApiUrl("reset_password")
        
        let params: [String : AnyObject] = [
            "password": currentPassTextField.text!,
            "new_password": newPassTextField.text!
        ]
        
        let response = Just.post(apiUrl,
            headers: ["Authorization": authToken],
            data: params
        )
        
        if response.ok {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
}
