//
//  MarkerInfoView.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {
    
    @IBOutlet weak var markerInfoImage: UIImageView!
    @IBOutlet weak var markerInfoTypeTitle: UILabel!
    @IBOutlet weak var markerInfoMovieName: UILabel!
    @IBOutlet weak var markerInfoStoreName: UILabel!
}
