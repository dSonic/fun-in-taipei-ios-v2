//
//  BookmarkViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/9/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class BookmarkViewController: UIViewController, UITableViewDataSource,
    UITableViewDelegate, MapFilterDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var bookmarks: [[String:AnyObject]] = []
    var isRefreshPage = true
    var isNextPage = false
    var nextPageUrl = ""
    var poiId: Int!
    var longitude: String!
    var latitude: String!
    var shouldReload = true
    
    var poiDetailInfo: NSDictionary!
    private var pleaseSignInVC: PleaseSigninViewController! = nil
    private var noContentVC: NoContentViewController! = nil
    
    let poiTypeIcons = [UIImage(named: "pin_food"), UIImage(named: "pin_food"), UIImage(named: "pin_store"), UIImage(named: "pin_attraction")]
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        cleanUpViews()
        
        let authToken = Auth.sharedAuth.authToken
        
        if authToken.isEmpty {
            tableView.hidden = true
            
            pleaseSignInVC = self.storyboard?.instantiateViewControllerWithIdentifier("PleaseSigninViewController") as! PleaseSigninViewController
            pleaseSignInVC.view.frame = self.view.frame
            self.addChildViewController(pleaseSignInVC)
            self.view.insertSubview(pleaseSignInVC.view, atIndex: 0)
            pleaseSignInVC!.didMoveToParentViewController(self)
        } else {
            if shouldReload == true {
                getBookmarks()
            }
            
            if bookmarks.isEmpty == true {
                tableView.hidden = true
                
                noContentVC = self.storyboard?.instantiateViewControllerWithIdentifier("NoContentVC") as! NoContentViewController
                noContentVC.view.frame = self.view.frame
                self.addChildViewController(noContentVC)
                self.view.insertSubview(noContentVC.view, atIndex: 0)
                noContentVC.didMoveToParentViewController(self)
            } else {
                tableView.hidden = false
            }
        }
    }
    
    private func cleanUpViews() {
        if pleaseSignInVC != nil {
            pleaseSignInVC.willMoveToParentViewController(nil)
            pleaseSignInVC.view.removeFromSuperview()
            pleaseSignInVC.removeFromParentViewController()
        }
        
        if noContentVC != nil {
            noContentVC.willMoveToParentViewController(nil)
            noContentVC.view.removeFromSuperview()
            noContentVC.removeFromParentViewController()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "我的收藏"
        drawSlideMenuButton()
        drawFilterButton()
    }
    
    private func getBookmarks() {
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        let apiUrl = ApiList.sharedApi.getApiUrl("bookmark")
        let response = Just.get(apiUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            bookmarks = response.json?.objectForKey("results") as! Array
            
            let nextPage = response.json?.objectForKey("next")
            
            if let _ = nextPage as? NSNull {
                isNextPage = false
            } else {
                isNextPage = true
                nextPageUrl = nextPage as! String
            }
            
            tableView.reloadData()
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    private func getMoreBookmarks() {
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        let response = Just.get(nextPageUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            let tmpBookmarks = response.json?.objectForKey("results") as! NSArray
            
            for bookmark in tmpBookmarks {
                bookmarks.append(bookmark as! Dictionary)
            }
            
            let nextPage = response.json?.objectForKey("next")
            
            if let _ = nextPage as? NSNull {
                isNextPage = false
            } else {
                isNextPage = true
                nextPageUrl = nextPage as! String
            }
            
            tableView.reloadData()
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // When user scroll to the bottom of the screen,
        // trigger the getMoreBookmarks method to get more bookmarks
        // if isNextPage == true
        if self.tableView.contentOffset.y >= self.tableView.contentSize.height - self.tableView.bounds.size.height {
            if isNextPage == true {
                isNextPage = false
                
                getMoreBookmarks()
            }
        }
    }
    
    func drawSlideMenuButton() {
        let addImage: UIImage? = UIImage(named: "slide_menu_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        
        let slideMenuButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        if self.revealViewController() != nil {
            addButton.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
            self.view.addGestureRecognizer((self.revealViewController().panGestureRecognizer()))
        }
        
        self.navigationItem.setLeftBarButtonItem(slideMenuButton, animated: false)
    }
    
    func drawFilterButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "filterButtonPressed:", forControlEvents: .TouchUpInside)
        
        let filterButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        self.navigationItem.setRightBarButtonItem(filterButton, animated: false)
    }
    
    func filterButtonPressed(sender: UIButton) {
        self.performSegueWithIdentifier("BookmarkToFilterSegue", sender: sender)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BookmarkCell", forIndexPath: indexPath) as! BookmarkTableViewCell
        
        cell.borderView.layer.borderWidth = 1.0
        let thumbImageUrl = bookmarks[indexPath.row]["thumbnail"] as! String
        cell.poiThumbImage.hnk_setImageFromURL(NSURL(string: thumbImageUrl)!)
        cell.poiNameLabel.text = bookmarks[indexPath.row]["name"] as? String
        cell.poiMovieLabel.text = bookmarks[indexPath.row]["movie"] as? String
        cell.poiAddressLabel.text = bookmarks[indexPath.row]["address"] as? String
        
        let poiType = bookmarks[indexPath.row]["type"] as! Int
        
        cell.poiTypeIcon.image = poiTypeIcons[poiType]
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 90
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "BookmarkToPoiDetailSegue" {
            let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
            
            poiDetailVC.poiDetailInfo = poiDetailInfo
            poiDetailVC.poiId = poiId
        } else if segue.identifier == "BookmarkToFilterSegue" {
            let mapFilterVC = segue.destinationViewController as! MapFilterViewController
            
            mapFilterVC.isFromBookmark = true
            mapFilterVC.delegate = self
            self.shouldReload = false // to avoid calling API each time view appeared
        }
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == "BookmarkToPoiDetailSegue" {
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            let selectedIndex = tableView.indexPathForSelectedRow?.row
            poiId = bookmarks[selectedIndex!]["id"] as! Int
            
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getPoiDetailApi(poiId)
            
            let response = Just.get(apiUrl, headers: ["Authorization": authToken])
            
            if response.ok {
                poiDetailInfo = response.json?.objectForKey("data") as! NSDictionary
                
                return true
            } else if response.statusCode == nil {
                let alert = Alert()
                alert.noConnectionAlert()
                
                return false
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
                
                return false
            }
        }
        
        return true
    }
    
    /// A Map Filter Delegate method to get bookmarks
    /// based on the filters the user selected.
    ///
    /// - Parameters:
    ///   - bookmarks: An `Array` of the bookmarks information
    ///   - isNextPage: A `Bool` that indicates if there is a next page
    /// needed to be loaded.
    ///   - nextPageUrl: A `String` to store the next page API url if any
    func updateBookmarkListByFilter(bookmarks: [[String : AnyObject]], isNextPage: Bool, nextPageUrl: String) {
        self.bookmarks = bookmarks
        self.isNextPage = isNextPage
        self.nextPageUrl = nextPageUrl
        
        tableView.reloadData()
    }

}
