//
//  TripPlannerWeatherViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol TripPlannerWeatherDelegate {
    func confirmButtonToggleFromWeather()
}

class TripPlannerWeatherViewController: UIViewController {
    @IBOutlet weak var coupleButton: UIButton!
    @IBOutlet weak var friendsButton: UIButton!
    @IBOutlet weak var familyButton: UIButton!
    
    private var buttonImages: Array<UIImage> = [
        UIImage(named: "trip_planner_couple_btn")!,
        UIImage(named: "trip_planner_couple_btn")!,
        UIImage(named: "trip_planner_friends_btn")!,
        UIImage(named: "trip_planner_family_btn")!,
    ]
    
    private var buttonPressedImages: Array<UIImage> = [
        UIImage(named: "trip_planner_couple_btn_pressed")!,
        UIImage(named: "trip_planner_couple_btn_pressed")!,
        UIImage(named: "trip_planner_friends_btn_pressed")!,
        UIImage(named: "trip_planner_family_btn_pressed")!,
    ]
    
    private let sharedSettings = TripPlannerSettings.sharedTripPlannerSettings
    private var settings: Dictionary<String,Int>!
    
    var delegate: TripPlannerWeatherDelegate! = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        settings = sharedSettings.settings as! Dictionary
        
        let imageIndex = settings["weather"]!
        
        let buttonImage = buttonPressedImages[imageIndex]
        var selectedButton: UIButton!
        
        switch imageIndex {
        case 1:
            selectedButton = coupleButton
        case 2:
            selectedButton = friendsButton
        case 3:
            selectedButton = familyButton
        default:
            selectedButton = nil
        }
        
        if selectedButton != nil {
            selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
        }
    }

    private func switchToButton(sender: UIButton) {
        let buttonImage = buttonPressedImages[sender.tag]
        var selectedButton: UIButton!
        
        switch sender.tag {
        case 1:
            selectedButton = coupleButton
            friendsButton.setBackgroundImage(buttonImages[2], forState: .Normal)
            familyButton.setBackgroundImage(buttonImages[3], forState: .Normal)
        case 2:
            selectedButton = friendsButton
            coupleButton.setBackgroundImage(buttonImages[1], forState: .Normal)
            familyButton.setBackgroundImage(buttonImages[3], forState: .Normal)
        case 3:
            selectedButton = familyButton
            coupleButton.setBackgroundImage(buttonImages[1], forState: .Normal)
            friendsButton.setBackgroundImage(buttonImages[2], forState: .Normal)
        default:
            selectedButton = coupleButton
            friendsButton.setBackgroundImage(buttonImages[2], forState: .Normal)
            familyButton.setBackgroundImage(buttonImages[3], forState: .Normal)
        }
        
        selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
    }
    
    @IBAction func coupleButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["weather"] = 1
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.confirmButtonToggleFromWeather()
    }
    
    @IBAction func friendsButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["weather"] = 2
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.confirmButtonToggleFromWeather()
    }

    @IBAction func familyButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["weather"] = 3
        sharedSettings.updateSettings(settings as NSDictionary)
        delegate.confirmButtonToggleFromWeather()
    }
}
