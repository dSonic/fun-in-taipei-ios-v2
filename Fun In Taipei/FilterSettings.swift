//
//  FilterSettings.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/17/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation

class FilterSettings {
    class var sharedFilterSettings : FilterSettings {
        struct Singleton {
            static let instance = FilterSettings()
        }
        
        return Singleton.instance
    }
    
    private(set) var settings: NSDictionary
    private(set) var locations: [String]
    
    init() {
        let defaults = NSUserDefaults.standardUserDefaults()
        let storedSettings = defaults.objectForKey("filter_settings") as? NSDictionary
        
        let initialSetting = ["location": 0, "food": true, "poi": true, "shopping": true]
        
        settings = storedSettings != nil ? storedSettings! : initialSetting
        locations = ["全部", "士林區", "大同區", "大安區", "中山區", "中正區", "內湖區",
            "文山區", "北投區", "松山區", "信義區", "南港區", "萬華區"]
    }
    
    func updateSettings(settings: NSDictionary) {
        self.settings = settings
        saveSettings()
    }
    
    func resetSettings() {
        self.settings = ["location": 0, "food": true, "poi": true, "shopping": true]
        saveSettings()
    }
    
    private func saveSettings() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.settings, forKey: "filter_settings")
        defaults.synchronize()
    }
}