//
//  NoContentViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 1/13/16.
//  Copyright © 2016 LBSTek Inc. All rights reserved.
//

import UIKit

class NoContentViewController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        messageLabel.text = "快去地圖模式看看附近有哪些好玩景點！把它們加入收藏吧！"
    }
}
