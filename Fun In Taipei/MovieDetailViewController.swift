//
//  MovieDetailViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class MovieDetailViewController: UIViewController, MovieDetailDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    
    var movieId: Int!
    var movieTitle: String! = ""
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = movieTitle
        
        drawBackButton()
        drawPoiListButton()
    }
    
    private func drawBackButton() {
        let btnImage = UIImage(named: "nav_back_btn")!
        let backButton = UIButton(type: .Custom)
        
        backButton.frame = CGRectMake(0, 0, btnImage.size.width, btnImage.size.height)
        backButton.setBackgroundImage(btnImage, forState: .Normal)
        backButton.addTarget(self, action: "backButtonPressed:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: backButton), animated: false)
    }
    
    func backButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func drawPoiListButton() {
        let addImage: UIImage? = UIImage(named: "map_poi_list_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "poiListButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    func poiListButtonPressed(sender: UIButton) {
        self.performSegueWithIdentifier("MoviePoiListSegue", sender: sender)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MovieDetailContentSegue" {
            let movieDetailContentVC = segue.destinationViewController as! MovieDetailContentViewController
            movieDetailContentVC.movieId = self.movieId
            movieDetailContentVC.delegate = self
        } else if segue.identifier == "MoviePoiListSegue" {
            let moviePoiListVC = segue.destinationViewController as! MoviePoiListTableViewController
            
            let sharedPoi = PoiModel.sharedPoiModel
            let poiList = sharedPoi.getData()
            
            for poi in poiList {
                if poi.objectForKey("movie_id") as! Int == movieId {
                    moviePoiListVC.poiList.append(poi)
                }
            }
        }
    }
    
    func resizeContentSize(height: CGFloat) {
        scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width, height)
    }

}
