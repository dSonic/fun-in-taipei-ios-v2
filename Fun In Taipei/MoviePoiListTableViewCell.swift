//
//  MoviePoiListTableViewCell.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/13/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class MoviePoiListTableViewCell: UITableViewCell {
    @IBOutlet weak var poiThumbImageView: UIImageView!
    @IBOutlet weak var poiTypeImageView: UIImageView!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var poiMovieLabel: UILabel!
    @IBOutlet weak var poiAddressLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
