//
//  PoiDetailViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class PoiDetailViewController: UIViewController {
    
    @IBOutlet weak var contentVC: PoiDetailContentViewController!
    @IBOutlet weak var poiIntroTab: UIButton!
    @IBOutlet weak var poiInfoTab: UIButton!
    @IBOutlet weak var poiPromotionTab: UIButton!
    @IBOutlet weak var poiRatingTab: UIButton!
    
    var poiDetailInfo: NSDictionary!
    var tappedMarker: PoiMarker!
    var isFavorite: Bool!
    var poiId: Int!
    var isPushed = true
    
    var authVC: AuthRootViewController!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        drawFavoriteButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = poiDetailInfo.objectForKey("name") as? String
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        
        drawBackButton()
        
        poiIntroTab.setTitleColor(UIColor.orangeColor(), forState: .Normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiDetailContentSegue" {
            self.contentVC = segue.destinationViewController as! PoiDetailContentViewController
            self.contentVC.poiDetailInfo = self.poiDetailInfo
            contentVC.tappedMarker = tappedMarker
            contentVC.poiId = poiId
        }
    }
    
    private func drawBackButton() {
        let btnImage = UIImage(named: "nav_back_btn")!
        let backButton = UIButton(type: .Custom)
        
        backButton.frame = CGRectMake(0, 0, btnImage.size.width, btnImage.size.height)
        backButton.setBackgroundImage(btnImage, forState: .Normal)
        backButton.addTarget(self, action: "backButtonPressed:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: backButton), animated: false)
    }
    
    private func drawFavoriteButton() {
        // To solve the problem of incorrect bookmark status being displayed
        // if the user has just logged in from the POI Deatil Views.
        let sharedAuth = Auth.sharedAuth
        let authToken = sharedAuth.authToken
        let sharedApi = ApiList.sharedApi
        
        //let poiId = tappedMarker.poi["id"] as! Int
        let apiUrl = sharedApi.getIsFavoriteApi(poiId)
        
        if authToken.isEmpty == false {
            let response = Just.get(apiUrl, headers: ["Authorization": "JWT \(authToken)"])
            
            if response.ok {
                isFavorite = response.json?.objectForKey("data")?.objectForKey("is_favorite") as! Bool
            } else if response.statusCode == 401 {
                let alertController = generateSigninAlertView()
                presentViewController(alertController, animated: true, completion: nil)
            }
        }
        
        // Chagne the Bookmark button image accordingly.
        var btnImage = UIImage(named: "poi_detail_favorite_btn")!
        
        if isFavorite == true {
            btnImage = UIImage(named: "poi_detail_favorite_true_btn")!
        }
        
        let favButton = UIButton(type: .Custom)
        
        favButton.frame = CGRectMake(0, 0, btnImage.size.width, btnImage.size.height)
        favButton.setBackgroundImage(btnImage, forState: .Normal)
        favButton.addTarget(self, action: Selector("bookmarkToggle"), forControlEvents: .TouchUpInside)
        
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(customView: favButton), animated: false)
    }
    
    func backButtonPressed(sender: UIButton) {
        if isPushed == true {
            self.navigationController?.popViewControllerAnimated(true)
        } else {
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    /*
    // MARK: - Switching Functions
    *
    * Navigation Tabs Switching Functions
    */
    @IBAction func poiInfoTabPressed(sender: UIButton) {
        poiIntroTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiPromotionTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiRatingTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        poiInfoTab.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        self.contentVC.swapToViewControllerWithIdentifier("PoiInfoSegue")
    }
    
    @IBAction func poiIntroTabPressed(sender: UIButton) {
        poiInfoTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiPromotionTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiRatingTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        poiIntroTab.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        self.contentVC.swapToViewControllerWithIdentifier("PoiIntroSegue")
    }
    
    @IBAction func poiPromotionTabPressed(sender: UIButton) {
        poiIntroTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiInfoTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiRatingTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        poiPromotionTab.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        self.contentVC.swapToViewControllerWithIdentifier("PoiPromotionSegue")
    }
    
    @IBAction func poiRatingTabPressed(sender: UIButton) {
        poiIntroTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiInfoTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        poiPromotionTab.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        poiRatingTab.setTitleColor(UIColor.orangeColor(), forState: .Normal)
        self.contentVC.swapToViewControllerWithIdentifier("PoiRatingSegue")
    }
    
    /**
     * This function is a selector function used for Bookmark Toogle Button.
     *
     * If the Bookmark toggle button is pressed, 
     * it will call the Favorite API to set or unset bookmark for users.
    */
    func bookmarkToggle() {
        let sharedAuth = Auth.sharedAuth
        let authToken = sharedAuth.authToken

        let signInAlertController = generateSigninAlertView()
        
        if authToken.isEmpty == true {
            presentViewController(signInAlertController, animated: true, completion: nil)
        } else {
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getApiUrl("bookmark")
            let authToken = "JWT \(Auth.sharedAuth.authToken)"
            //let poiId = tappedMarker.poi["id"] as! Int
            
            let response = Just.post(apiUrl, data: ["poi_id": poiId], headers: ["Authorization": authToken])
            
            if response.ok {
                isFavorite = !isFavorite
                drawFavoriteButton()
            } else if response.statusCode == 401 { // if token is expired. just in case...
                sharedAuth.setAuthToken("")
                presentViewController(signInAlertController, animated: true, completion: nil)
            }
        }
    }
    
    /**
     * This function to show the Login Alert View 
     * if the user has pressed the bookmark button without login.
    */
    func generateSigninAlertView() -> UIAlertController {
        let alertController = UIAlertController(title: "溫馨提醒",
            message: "此功能需要登入後才能使用...", preferredStyle: .Alert)
        let signInAction = UIAlertAction(title: "立即登入", style: .Destructive, handler: {action in
            self.authVC = self.storyboard?.instantiateViewControllerWithIdentifier("AuthRootViewController") as! AuthRootViewController
            self.presentViewController(self.authVC, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "暫不登入", style: .Cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(signInAction)
        
        return alertController
    }

}
