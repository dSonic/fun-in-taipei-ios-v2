//
//  TripPlannerResultTableViewCell.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/16/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class TripPlannerResultTableViewCell: UITableViewCell {
    @IBOutlet weak var poiThumbImageView: UIImageView!
    @IBOutlet weak var poiTypeImageView: UIImageView!
    @IBOutlet weak var poiNameLabel: UILabel!
    @IBOutlet weak var movieNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
