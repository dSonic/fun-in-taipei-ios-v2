//
//  MapFilterViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/16/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

protocol MapFilterDelegate {
    func updateBookmarkListByFilter(
        bookmarks: [[String : AnyObject]], isNextPage: Bool, nextPageUrl: String)
}

class MapFilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var foodTypeButton: UIButton!
    @IBOutlet weak var poiTypeButton: UIButton!
    @IBOutlet weak var shoppingTypeButton: UIButton!
    
    var locations:[String] = []
    var bookmarks: [[String:AnyObject]] = []
    
    let filterSettings = FilterSettings.sharedFilterSettings
    var localFilterSettings = ["food": false, "poi": false, "shopping": false, "location": 0]
    var isFoodSelected:Bool = false
    var isPoiSelected:Bool = false
    var isShoppingSelected:Bool = false
    
    let foodButtonImage = UIImage(named: "map_filter_food_btn")
    let foodButtonImageSelected = UIImage(named: "map_filter_food_selected")
    let poiButtonImage = UIImage(named: "map_filter_poi_btn")
    let poiButtonImageSelected = UIImage(named: "map_filter_poi_btn_selected")
    let shoppingButtonImage = UIImage(named: "map_filter_shopping_btn")
    let shoppingButtonImageSelected = UIImage(named: "map_filter_shopping_btn_selected")
    
    var isFromBookmark = false
    var delegate: MapFilterDelegate! = nil

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        locations = filterSettings.locations
        let selectedLocationTitle = locations[0]
        
        locationButton.setTitle(selectedLocationTitle, forState: .Normal)
        
        /*
        // load the filter settings from the user defaults file
        self.localFilterSettings = self.filterSettings.settings as! Dictionary
        self.locations = self.filterSettings.locations
        
        let selectedLocation = self.filterSettings.settings.valueForKey("location") as! NSInteger
        let selectedLocationTitle = self.locations[selectedLocation]
        
        self.locationButton.setTitle(selectedLocationTitle, forState: .Normal)

        self.isFoodSelected = self.filterSettings.settings.valueForKey("food") as! Bool
        self.isPoiSelected = self.filterSettings.settings.valueForKey("poi") as! Bool
        self.isShoppingSelected = self.filterSettings.settings.valueForKey("shopping") as! Bool
        
        if self.isFoodSelected {
            self.foodTypeButton.setBackgroundImage(self.foodButtonImageSelected, forState: .Normal)
        } else {
            self.foodTypeButton.setBackgroundImage(self.foodButtonImage, forState: .Normal)
        }
        
        if self.isPoiSelected {
            self.poiTypeButton.setBackgroundImage(self.poiButtonImageSelected, forState: .Normal)
        } else {
            self.poiTypeButton.setBackgroundImage(self.poiButtonImage, forState: .Normal)
        }
        
        if self.isShoppingSelected {
            self.shoppingTypeButton.setBackgroundImage(self.shoppingButtonImageSelected, forState: .Normal)
        } else {
            self.shoppingTypeButton.setBackgroundImage(self.shoppingButtonImage, forState: .Normal)
        }*/
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "景點篩選"
        
        drawConfirmButton()
        drawCancelButton()
        
        self.navigationItem.rightBarButtonItem?.enabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func drawConfirmButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_confirm_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "confirmButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func confirmButtonPressed(sender: UIButton) {
        // Because the Map Filter View is being shared by both
        // Bookmark and Map View. So need a isFromBookmark flag to
        // determine which view is coming from in order to 
        // decide which action to take.
        if isFromBookmark == false {
            if localFilterSettings["food"] == true || localFilterSettings["poi"] == true || localFilterSettings["shopping"] == true {
                self.filterSettings.updateSettings(self.localFilterSettings)
            }
        } else {
            var types = ""
            
            if localFilterSettings["food"] == 1 {
                types += "1,"
            }
            
            if localFilterSettings["shopping"] == 1 {
                types += "2,"
            }
            
            if localFilterSettings["poi"] == 1 {
                types += "3,"
            }
            
            let index = types.endIndex.advancedBy(-1)
            let newTypes = types.substringToIndex(index)
            
            let area = localFilterSettings["location"] as! Int
            
            let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
            
            let apiUrl = ApiList.sharedApi.getBookmarkFilterApi(newTypes, area: area)
            
            let response = Just.get(apiUrl, headers: ["Authorization": authToken])
            
            if response.ok {
                let data = response.json as! NSDictionary
                bookmarks = data.objectForKey("results") as! [[String : AnyObject]]
                
                let nextPage = data.objectForKey("next")
                var isNextPage = true
                var nextPageUrl = ""
                
                if let _ = nextPage as? NSNull {
                    isNextPage = false
                } else {
                    isNextPage = true
                    nextPageUrl = nextPage as! String
                }
                
                delegate.updateBookmarkListByFilter(bookmarks, isNextPage: isNextPage, nextPageUrl: nextPageUrl)
            } else {
                let alert = Alert()
                alert.unknownServerErrorAlert()
            }
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    private func confirmButtonToggle() {
        if localFilterSettings["food"] == true || localFilterSettings["poi"] == true || localFilterSettings["shopping"] == true {
            self.navigationItem.rightBarButtonItem?.enabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
    }
    
    @IBAction func locationButtonPressed(sender: UIButton) {
        self.tableView.hidden = (self.tableView.hidden) ? false : true
    }
    
    @IBAction func foodButtonPressed(sender: UIButton) {
        self.isFoodSelected = !self.isFoodSelected
        self.localFilterSettings["food"] = self.isFoodSelected
        
        let btnImage = self.isFoodSelected ? self.foodButtonImageSelected : self.foodButtonImage
        self.foodTypeButton.setBackgroundImage(btnImage, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func poiButtonPressed(sender: UIButton) {
        self.isPoiSelected = !self.isPoiSelected
        self.localFilterSettings["poi"] = self.isPoiSelected
        
        let btnImage = self.isPoiSelected ? self.poiButtonImageSelected : self.poiButtonImage
        self.poiTypeButton.setBackgroundImage(btnImage, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    @IBAction func shoppingButtonPressed(sender: UIButton) {
        self.isShoppingSelected = !self.isShoppingSelected
        self.localFilterSettings["shopping"] = self.isShoppingSelected
        
        let btnImage = self.isShoppingSelected ? self.shoppingButtonImageSelected : self.shoppingButtonImage
        self.shoppingTypeButton.setBackgroundImage(btnImage, forState: .Normal)
        
        confirmButtonToggle()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.locations.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MapFilterLocationCell")!
        
        cell.textLabel?.textAlignment = NSTextAlignment.Center
        cell.textLabel?.text = self.locations[indexPath.row]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView = tableView
        
        let selectedLocationTitle = self.locations[indexPath.row]
        self.locationButton.setTitle(selectedLocationTitle, forState: .Normal)
        
        self.localFilterSettings["location"] = indexPath.row
        
        self.tableView.hidden = (self.tableView.hidden) ? false : true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
