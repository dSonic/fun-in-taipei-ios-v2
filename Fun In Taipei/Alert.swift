//
//  Alert.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    func noConnectionAlert() {
        let alert = UIAlertView(title: "網路錯誤", message: "請檢查網路連線...",
            delegate: self, cancelButtonTitle: "確認")
        alert.show()
    }
    
    func unknownServerErrorAlert() {
        let alert = UIAlertView(title: "不明錯誤", message: "非常抱歉，伺服器產生不明錯誤，請稍候再試...",
            delegate: self, cancelButtonTitle: "確認")
        alert.show()
    }
}
