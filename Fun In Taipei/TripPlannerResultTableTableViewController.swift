//
//  TripPlannerResultTableTableViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/15/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class TripPlannerResultTableTableViewController: UITableViewController {
    var tourList: NSDictionary!
    var tourPois: [[String : AnyObject]]!
    var recommendPois: [[String : AnyObject]]!
    var days: Int!
    var typeImages: Array<String> = ["", "pin_food", "pin_store", "pin_attraction"]
    var poiId = 0
    var isFavorite = false
    var poiDetailinfo: NSDictionary! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        tourPois = tourList.objectForKey("tour_pois") as! [[String : AnyObject]]
        recommendPois = tourList.objectForKey("recommend_pois") as! [[String: AnyObject]]
        
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "推薦列表"
        
        drawBackButton()
    }

    private func drawBackButton() {
        let btnImage = UIImage(named: "nav_back_btn")!
        let backButton = UIButton(type: .Custom)
        
        backButton.frame = CGRectMake(0, 0, btnImage.size.width, btnImage.size.height)
        backButton.setBackgroundImage(btnImage, forState: .Normal)
        backButton.addTarget(self, action: "backButtonPressed:", forControlEvents: .TouchUpInside)
        
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: backButton), animated: false)
    }
    
    func backButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if days == 2 {
            return 4
        } else {
            return 3
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Changing number of rows base on the Days selected.
        // There should be a better way I think?
        if section == 0 {
            return 1
        }
        
        if days == 0 {
            if section == 1 {
                return 2
            }
            
            if section == 2 {
                return recommendPois.count
            }
        }
        
        if days == 1 {
            if section == 1 {
                return 6
            }
            
            if section == 2 {
                return recommendPois.count
            }
        }
        
        if days == 2 {
            if section == 1 || section == 2 {
                return 6
            }
            
            if section == 3 {
                return recommendPois.count
            }
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let emptyCell = UITableViewCell()
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultDescriptionCell", forIndexPath: indexPath) as! TripPlannerResultDescriptionTableViewCell
            cell.descriptionLabel.text = "以下為您的一日遊推薦行程,\n如果想旅程更佳豐富,\n可以參考其他推薦的景點喔！"
            
            return cell
        }
        
        // Again, returngin different rows base on the Days selected.
        if days == 0 || days == 1 {
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultCell", forIndexPath: indexPath) as! TripPlannerResultTableViewCell
                
                let thumbImageUrl = tourPois[indexPath.row]["thumbnail"] as? String
                let typeId = tourPois[indexPath.row]["type"] as? Int
                let imageName = typeImages[typeId!]
                
                cell.poiNameLabel.text = tourPois[indexPath.row]["name"] as? String
                cell.movieNameLabel.text = tourPois[indexPath.row]["movie"] as? String
                cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbImageUrl!)!)
                cell.poiTypeImageView.image = UIImage(named: imageName)
                
                return cell
            }
            
            if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultCell", forIndexPath: indexPath) as! TripPlannerResultTableViewCell
                
                let thumbImageUrl = recommendPois[indexPath.row]["thumbnail"] as? String
                let typeId = recommendPois[indexPath.row]["type"] as? Int
                let imageName = typeImages[typeId!]
                
                cell.poiNameLabel.text = recommendPois[indexPath.row]["name"] as? String
                cell.movieNameLabel.text = recommendPois[indexPath.row]["movie"] as? String
                cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbImageUrl!)!)
                cell.poiTypeImageView.image = UIImage(named: imageName)
                
                return cell
            }
        }
        
        if days == 2 {
            if indexPath.section == 1 {
                let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultCell", forIndexPath: indexPath) as! TripPlannerResultTableViewCell
                
                let thumbImageUrl = tourPois[indexPath.row]["thumbnail"] as? String
                let typeId = tourPois[indexPath.row]["type"] as? Int
                let imageName = typeImages[typeId!]
                
                cell.poiNameLabel.text = tourPois[indexPath.row]["name"] as? String
                cell.movieNameLabel.text = tourPois[indexPath.row]["movie"] as? String
                cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbImageUrl!)!)
                cell.poiTypeImageView.image = UIImage(named: imageName)
                
                return cell
            }
            
            if indexPath.section == 2 {
                let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultCell", forIndexPath: indexPath) as! TripPlannerResultTableViewCell
                
                let thumbImageUrl = tourPois[indexPath.row+6]["thumbnail"] as? String
                let typeId = tourPois[indexPath.row]["type"] as? Int
                let imageName = typeImages[typeId!]
                
                cell.poiNameLabel.text = tourPois[indexPath.row+6]["name"] as? String
                cell.movieNameLabel.text = tourPois[indexPath.row+6]["movie"] as? String
                cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbImageUrl!)!)
                cell.poiTypeImageView.image = UIImage(named: imageName)
                
                return cell
            }
            
            if indexPath.section == 3 {
                let cell = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultCell", forIndexPath: indexPath) as! TripPlannerResultTableViewCell
                
                let thumbImageUrl = recommendPois[indexPath.row]["thumbnail"] as? String
                let typeId = recommendPois[indexPath.row]["type"] as? Int
                let imageName = typeImages[typeId!]
                
                cell.poiNameLabel.text = recommendPois[indexPath.row]["name"] as? String
                cell.movieNameLabel.text = recommendPois[indexPath.row]["movie"] as? String
                cell.poiThumbImageView.hnk_setImageFromURL(NSURL(string: thumbImageUrl!)!)
                cell.poiTypeImageView.image = UIImage(named: imageName)
                
                return cell
            }
        }
        
        return emptyCell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 70.0
        } else {
            return 90.0
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCellWithIdentifier("TripPlannerResultHeader") as! TripPlannerResultHeaderTableViewCell
        
        if section == 0 {
            headerView.titleLabel.text = "推薦行程"
        }
        
        if days == 0 {
            if section == 1 {
                headerView.titleLabel.text = "半日行程"
            }
            
            if section == 2 {
                headerView.titleLabel.text = "其他推薦"
            }
        }
        
        if days == 1 {
            if section == 1 {
                headerView.titleLabel.text = "一日行程"
            }
            
            if section == 2 {
                headerView.titleLabel.text = "其他推薦"
            }
        }
        
        if days == 2 {
            if section == 1 {
                headerView.titleLabel.text = "一日行程"
            }
            
            if section == 2 {
                headerView.titleLabel.text = "二日行程"
            }
            
            if section == 3 {
                headerView.titleLabel.text = "其他推薦"
            }
        }
        
        return headerView
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        let indexPath = tableView.indexPathForSelectedRow!
        
        if days == 0 || days == 1 {
            if indexPath.section == 1 {
                poiId = tourPois[indexPath.row]["id"] as! Int
            }
            
            if indexPath.section == 2 {
                poiId = recommendPois[indexPath.row]["id"] as! Int
            }
        }
        
        if days == 2 {
            if indexPath.section == 1 {
                poiId = tourPois[indexPath.row]["id"] as! Int
            }
            
            if indexPath.section == 2 {
                poiId = tourPois[indexPath.row+6]["id"] as! Int
            }
            
            if indexPath.section == 3 {
                poiId = recommendPois[indexPath.row]["id"] as! Int
            }
        }
        
        let sharedAuth = Auth.sharedAuth
        let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
        
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getPoiDetailApi(poiId)
        
        let response = Just.get(apiUrl, headers: ["Authorization": authToken])
        
        if response.ok {
            poiDetailinfo = response.json?.objectForKey("data") as! NSDictionary
            return true
        } else if response.statusCode == nil {
            return false
        } else {
            return false
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TripPlannerResultToPoiDetailSegue" {
            let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
            poiDetailVC.poiDetailInfo = poiDetailinfo
            poiDetailVC.poiId = poiId
            poiDetailVC.isFavorite = poiDetailinfo.objectForKey("is_favorite") as! Bool
        }
    }

}
