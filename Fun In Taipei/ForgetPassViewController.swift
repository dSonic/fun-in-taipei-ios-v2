//
//  ForgetPassViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/25/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just
import SwiftValidator

protocol ForgetPassViewDelegate {
    func switchFromForgetPassToSignUp()
    func switchFromForgetPassToSignIn()
}

class ForgetPassViewController: UIViewController, ValidationDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var signUpTab: UIButton!
    @IBOutlet weak var signInTab: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var emailErrorLabel: UILabel!
    
    var delegate: ForgetPassViewDelegate! = nil
    
    let validator = Validator()

    override func viewDidLoad() {
        super.viewDidLoad()

        let lineView = UIView(frame: CGRectMake(self.signInTab.frame.size.width, 0, 1, self.signInTab.frame.size.height))
        lineView.backgroundColor = UIColor.blackColor()
        
        let lineView2 = UIView(frame: CGRectMake(self.signUpTab.frame.size.width, 0, 1, self.signUpTab.frame.size.height))
        lineView2.backgroundColor = UIColor.blackColor()
        
        self.signInTab.addSubview(lineView)
        self.signUpTab.addSubview(lineView2)
        
        emailTextField.delegate = self
        emailTextField.becomeFirstResponder()
        
        let label: UILabel = UILabel()
        label.font = UIFont(name: "DFWZMing-W4-WIN-BF", size: 24)
        label.textColor = UIColor.whiteColor()
        label.text = "登入/註冊"
        label.sizeToFit()
        
        self.navigationItem.titleView = label
        
        drawCancelButton()
        
        self.navBar.pushNavigationItem(self.navigationItem, animated: false)
        
        validator.registerField(emailTextField, errorLabel: emailErrorLabel,
            rules: [RequiredRule(message: "請輸入Email"), EmailRule(message: "Email格式錯誤")])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func signUpButtonPressed(sender: UIButton) {
        delegate.switchFromForgetPassToSignUp()
    }

    @IBAction func signInButtonPressed(sender: UIButton) {
        delegate.switchFromForgetPassToSignIn()
    }

    @IBAction func sendButtonPressedd(sender: UIButton) {
        // clean up error messages if any
        emailTextField.layer.borderColor = UIColor.blackColor().CGColor
        emailTextField.layer.borderWidth = 0.1
        emailErrorLabel.hidden = true
        
        // use the validator library
        validator.validate(self)
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in validator.errors {
            field.layer.borderColor = UIColor.redColor().CGColor
            field.layer.borderWidth = 1.0
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.hidden = false
        }
    }
    
    func validationSuccessful() {
        let sharedApi = ApiList.sharedApi
        let apiUrl = sharedApi.getApiUrl("forgetPass")
        
        let response = Just.post(
            apiUrl,
            data: ["email": emailTextField.text!]
        )
        
        if response.ok {
            let alert = UIAlertView(
                title: "成功送出", message: "已寄了一組臨時密碼到您的信箱，請登入後馬上修改...",
                delegate: self, cancelButtonTitle: "確認")
            alert.show()
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = UIAlertView(title: "傳送失敗", message: "請確認您輸入的Email是否正確...",
                delegate: self, cancelButtonTitle: "確認")
            alert.show()
        }
    }
    
}
