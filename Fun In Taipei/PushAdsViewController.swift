//
//  PushAdsViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 2/2/16.
//  Copyright © 2016 LBSTek Inc. All rights reserved.
//

import UIKit
import Haneke

class PushAdsViewController: UIViewController {
    var adsData: NSDictionary! = nil
    
    @IBOutlet weak var adsImageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if adsData != nil {
            let adsImageUrlString = adsData.objectForKey("image") as! String
            adsImageView.hnk_setImageFromURL(NSURL(string: adsImageUrlString)!)
            
            let adsImageRecognizer = UITapGestureRecognizer(target: self, action: Selector("adsImagePressed:"))
            adsImageView.addGestureRecognizer(adsImageRecognizer)
        }
    }
    
    @IBAction func closeButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func adsImagePressed(recognizer: UITapGestureRecognizer) {
        let adsUrlString = adsData.objectForKey("link") as! String
        UIApplication.sharedApplication().openURL(NSURL(string: adsUrlString)!)
    }
    
}
