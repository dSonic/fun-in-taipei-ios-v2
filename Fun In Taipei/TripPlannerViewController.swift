//
//  TripPlannerViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/19/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class TripPlannerViewController: UIViewController {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.backgroundImage = UIImage(named: "tabBar_trip_planner")
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "行程規劃"
        
        descriptionLabel.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        descriptionLabel.text = "依據電影，日程，季節，\n氣氛，景點類型\n規劃出最適合的行程，一起走進臺北\n走進電影的世界。"
        startButton.titleLabel?.font = UIFont(name: "DFWZMingW4-B5", size: 19)
        
        drawSlideMenuButton()
    }

    func drawSlideMenuButton() {
        let addImage: UIImage? = UIImage(named: "slide_menu_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        
        let slideMenuButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        if self.revealViewController() != nil {
            addButton.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
            self.view.addGestureRecognizer((self.revealViewController().panGestureRecognizer()))
        }
        
        self.navigationItem.setLeftBarButtonItem(slideMenuButton, animated: false)
    }

}
