//
//  MovieDetailContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

protocol MovieDetailDelegate {
    func resizeContentSize(height: CGFloat)
}

class MovieDetailContentViewController: UIViewController {
    var movieId: Int! = 0
    var movieTitle: String! = ""
    var isLayoutSubview = false
    var data: NSDictionary!
    
    let movieTypes = ["愛情", "喜劇", "動作", "劇情", "記錄", "恐怖"]
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var onTheaterDateLabel: UILabel!
    @IBOutlet weak var movieTypesLabel: UILabel!
    @IBOutlet weak var revenueLabel: UILabel!
    @IBOutlet weak var directorLabel: UILabel!
    @IBOutlet weak var producerLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var characterLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var delegate: MovieDetailDelegate! = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let apiUrl = ApiList.sharedApi.getMovieDetailApi(movieId)
        
        let response = Just.get(apiUrl)
        
        if response.ok {
            data = response.json?.objectForKey("data") as! NSDictionary
 
            let photos = data.objectForKey("photos") as! NSArray
            
            var imageUrl = ""
            
            if photos.count > 0 {
                imageUrl = photos[0].objectForKey("image") as! String
                movieImageView.hnk_setImageFromURL(NSURL(string: imageUrl)!)
            }
            
            let revenue = data.objectForKey("revenue") as! Int
            
            let formmatter = NSNumberFormatter()
            formmatter.numberStyle = .CurrencyStyle
            formmatter.locale = NSLocale(localeIdentifier: "zh_TW")
            
            let director = data.objectForKey("director") as? String
            let producer = data.objectForKey("producer") as? String
            let writer = data.objectForKey("writer") as? String
            let character = data.objectForKey("character") as? String
            
            onTheaterDateLabel.text = data.objectForKey("on_theater_date") as? String
            revenueLabel.text = revenue > 0 ? formmatter.stringFromNumber(NSNumber(integer: revenue)) : "暫無資料"
            directorLabel.text = director?.isEmpty == false ? director : "暫無資料"
            producerLabel.text = producer?.isEmpty == false ? producer : "暫無資料"
            writerLabel.text = writer?.isEmpty == false ? writer : "暫無資料"
            characterLabel.text = character?.isEmpty == false ? character : "暫無資料"
            descriptionLabel.text = data.objectForKey("description") as? String
            
            let types = data.objectForKey("types") as! Array<Int>
            var typesString = ""
            
            for type in types {
                typesString += "\(movieTypes[type - 1]) "
            }
            
            movieTypesLabel.text = typesString
        }
    }
    
    override func viewDidLayoutSubviews() {
        if isLayoutSubview == false {
            let contentHeight = descriptionLabel.frame.size.height + descriptionLabel.frame.origin.y + 20
            delegate.resizeContentSize(contentHeight)
            
            isLayoutSubview = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func watchVideoButtonPressed(sender: UIButton) {
        let videoUrl = data.objectForKey("trailer") as? String
        UIApplication.sharedApplication().openURL(NSURL(string: videoUrl!)!)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
