//
//  PoiDetailContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PoiDetailContentViewController: UIViewController {
    let PoiIntroSegueIdentifier = "PoiIntroSegue"
    let PoiInfoSegueIdentifier = "PoiInfoSegue"
    let PoiPromotionSegueIdentifier = "PoiPromotionSegue"
    let PoiRatingSegueIdentifier = "PoiRatingSegue"
    
    private var currentSegueIdentifier: String!
    
    var poiDetailInfo: NSDictionary!
    var tappedMarker: PoiMarker!
    var poiId: Int!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        currentSegueIdentifier = PoiIntroSegueIdentifier
        self.performSegueWithIdentifier(currentSegueIdentifier, sender: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == PoiIntroSegueIdentifier {
            if self.childViewControllers.count > 0 {
                let poiIntroVC = segue.destinationViewController as! PoiIntroViewController
                poiIntroVC.poiDetailInfo = self.poiDetailInfo
                
                self.swapFromViewController(self.childViewControllers[0],toViewController: segue.destinationViewController)
            } else {
                let poiIntroVC = segue.destinationViewController as! PoiIntroViewController
                poiIntroVC.poiDetailInfo = self.poiDetailInfo
                
                self.addChildViewController(segue.destinationViewController)
                segue.destinationViewController.view.frame = self.view.frame
                self.view.addSubview(segue.destinationViewController.view)
                segue.destinationViewController .didMoveToParentViewController(self)
            }
        } else if segue.identifier == PoiInfoSegueIdentifier {
            let poiInfoVC = segue.destinationViewController as! PoiInfoViewController
            poiInfoVC.poiDetailInfo = self.poiDetailInfo
            poiInfoVC.tappedMarker = tappedMarker
            poiInfoVC.poiId = poiId

            self.swapFromViewController(self.childViewControllers[0],toViewController: segue.destinationViewController)
        } else if segue.identifier == PoiPromotionSegueIdentifier {
            self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
        } else if segue.identifier == PoiRatingSegueIdentifier {
            let poiRatingVC = segue.destinationViewController as! PoiRatingViewController
            poiRatingVC.poiId = poiId
            poiRatingVC.poiDetailInfo = poiDetailInfo
            
            self.swapFromViewController(self.childViewControllers[0], toViewController: segue.destinationViewController)
        }
    }
    
    private func swapFromViewController(fromViewController:UIViewController, toViewController:UIViewController) {
        fromViewController.willMoveToParentViewController(nil)
        fromViewController.view.removeFromSuperview()
        fromViewController.removeFromParentViewController()
        
        toViewController.view.frame = self.view.frame
        self.addChildViewController(toViewController)
        self.view.insertSubview(toViewController.view, atIndex: 0)
        toViewController.didMoveToParentViewController(self)
    }
    
    func swapToViewControllerWithIdentifier(identifier: String) {
        currentSegueIdentifier = identifier
        self.performSegueWithIdentifier(currentSegueIdentifier, sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
