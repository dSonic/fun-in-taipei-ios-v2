//
//  PoiRatingEditorViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/21/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class PoiRatingEditorViewController: UIViewController, PoiRatingEditorContentDelegate {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var poiId = 0
    var poiDetailInfo: NSDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let label: UILabel = UILabel()
        label.font = UIFont(name: "DFWZMing-W4-WIN-BF", size: 24)
        label.textColor = UIColor.whiteColor()
        label.text = poiDetailInfo.objectForKey("name") as? String
        label.sizeToFit()
        
        self.navigationItem.titleView = label
        
        drawCancelButton()
        drawConfirmButton()
        
        navBar.pushNavigationItem(self.navigationItem, animated: false)
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let cancelButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(cancelButton, animated: true)
    }
    
    func drawConfirmButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_confirm_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "confirmButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func confirmButtonPressed(sender: UIButton) {
        let ratingEditorVC = self.childViewControllers[0] as! PoiRatingEditorContentViewController
        ratingEditorVC.confirmButtonPressed()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PoiRatingContentSegue" {
            let ratingEditorVC = segue.destinationViewController as! PoiRatingEditorContentViewController
            
            ratingEditorVC.poiId = poiId
            ratingEditorVC.delegate = self
        }
    }
    
    func contentHeightOfContainer(height: CGFloat) {
        scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width, height)
    }
    
    func reloadRatingsTable() {
        // nothing to do
    }
}
