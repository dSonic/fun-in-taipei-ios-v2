//
//  MovieViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/10/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Just

class MovieViewController: UIViewController, iCarouselDelegate, iCarouselDataSource, MovieFilterDelegate {
    
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieYearLabel: UILabel!
    
    var movies: Array<NSDictionary> = []
    var movieId: Int! = 0
    
    override func viewWillAppear(animated: Bool) {
        // Changed the background image of TabBar
        // to match the view every time the view is loaded.
        self.tabBarController?.tabBar.backgroundImage = UIImage(named: "tabBar_movie")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController!.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont(name: "DFWZMingW4-B5", size: 19)!]
        self.navigationItem.title = "電影列表"
        
        drawSlideMenuButton()
        drawFilterButton()
        
        fetchMovieList()
        
        carousel.delegate = self
        carousel.dataSource = self
        carousel.type = .CoverFlow
    }
    
    /**
     * Draw a custom Slide Menu Button ont left of the Navigation Bar
     */
    
    func drawSlideMenuButton() {
        let addImage: UIImage? = UIImage(named: "slide_menu_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        
        let slideMenuButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        if self.revealViewController() != nil {
            addButton.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
            self.view.addGestureRecognizer((self.revealViewController().panGestureRecognizer()))
        }
        
        self.navigationItem.setLeftBarButtonItem(slideMenuButton, animated: false)
    }
    
    func drawFilterButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "filterButtonPressed:", forControlEvents: .TouchUpInside)
        
        let filterButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        self.navigationItem.setRightBarButtonItem(filterButton, animated: false)
    }
    
    func filterButtonPressed(sender: UIButton) {
        self.performSegueWithIdentifier("MovieFilterSegue", sender: sender)
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return movies.count
    }
    
    func fetchMovieList(queryString: String = "") {
        var apiUrl: String!
        
        if queryString.isEmpty == true {
            apiUrl = ApiList.sharedApi.getApiUrl("movies")
        } else {
            apiUrl = ApiList.sharedApi.getMovieFilterApi(queryString)
        }
        
        let response = Just.get(apiUrl)
        
        if response.ok {
            
            movies = response.json?.objectForKey("data") as! Array
            
            carousel.reloadData()
            
            // make sure the movie title and year is displayed correctly
            // when coming back from the detail view.
            var currentIndex = carousel.currentItemIndex
            
            if currentIndex <= 0 {
                carousel.scrollToItemAtIndex(0, animated: false)
                currentIndex = 0
            }
            
            movieNameLabel.text = movies[currentIndex].objectForKey("name") as? String
            movieYearLabel.text = movies[currentIndex].objectForKey("year") as? String
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        let images = movies[index].objectForKey("images") as! Array<NSDictionary>
        let imageUrl = images[0].objectForKey("image") as! String
        
        let imageView: UIImageView

        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 250))
        imageView.hnk_setImageFromURL(NSURL(string: imageUrl)!)
        imageView.contentMode = UIViewContentMode.Center
        
        return imageView
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        movieId = movies[index].objectForKey("id") as! Int
        self.performSegueWithIdentifier("MovieDetailSegue", sender: nil)
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        // Changing the movie title and year while scrolling
        if carousel.currentItemIndex >= 0 { // to avoid the minus index when the page first loaded
            movieNameLabel.text = movies[carousel.currentItemIndex].objectForKey("name") as? String
            movieYearLabel.text = movies[carousel.currentItemIndex].objectForKey("year") as? String
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MovieDetailSegue" {
            let movieDetailVC = segue.destinationViewController as! MovieDetailViewController
            movieDetailVC.movieId = self.movieId
            movieDetailVC.movieTitle = self.movieNameLabel.text
            
        } else if segue.identifier == "MovieFilterSegue" {
            let movieFilterVC = segue.destinationViewController as! MovieFilterViewController
            movieFilterVC.delegate = self
        }
    }
    
    func updateMovieListByFilter(movieList: Array<NSDictionary>) {
        self.movies = movieList
        self.carousel.reloadData()
    }

}
