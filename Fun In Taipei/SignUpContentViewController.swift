//
//  SignUpContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/2/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import SwiftValidator

class SignUpContentViewController: UIViewController, ValidationDelegate, UITextFieldDelegate {

    @IBOutlet weak var accountTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var accountErrorLabel: UILabel!
    @IBOutlet weak var passErrorLabel: UILabel!
    @IBOutlet weak var confirmPassErrorLabel: UILabel!
    @IBOutlet weak var lastNameErrorLabel: UILabel!
    @IBOutlet weak var firstNameErrorLabel: UILabel!
    @IBOutlet weak var phoneErrorLabel: UILabel!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountTextField.delegate = self
        passwordTextField.delegate = self
        passwordConfirmTextField.delegate = self
        
        validator.registerField(accountTextField, errorLabel: accountErrorLabel,
            rules: [RequiredRule(message: "請輸入Eamil"), EmailRule(message: "請輸入正確的email")])
        validator.registerField(passwordTextField, errorLabel: passErrorLabel,
            rules: [RequiredRule(message: "請輸入密碼"), MaxLengthRule(length: 12, message: "密碼長度不能大於12"),
                MinLengthRule(length: 8, message: "密碼長度不能小於8")])
        validator.registerField(passwordConfirmTextField, errorLabel: confirmPassErrorLabel, rules: [ConfirmationRule(confirmField: passwordTextField, message: "密碼不符合")])
        validator.registerField(firstNameTextField, errorLabel: firstNameErrorLabel, rules: [RequiredRule(message: "請輸入姓氏")])
        validator.registerField(lastNameTextField, errorLabel: lastNameErrorLabel, rules: [RequiredRule(message: "請輸入名字")])
        validator.registerField(phoneTextField, errorLabel: phoneErrorLabel, rules: [RequiredRule(message: "請輸入電話號碼")])
        
        let sharedAuth = Auth.sharedAuth
        
        if sharedAuth.debug == true {
            accountTextField.text = "dsonic@gmail.com"
            passwordTextField.text = "asdfghjk"
            passwordConfirmTextField.text = "asdfghjk"
            firstNameTextField.text = "David"
            lastNameTextField.text = "Tung"
            phoneTextField.text = "0983658996"
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        accountTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signUpButtonPressed(sender: UIButton) {
        // Clear all the error messages if any
        accountTextField.layer.borderColor = UIColor.blackColor().CGColor
        accountTextField.layer.borderWidth = 0.1
        accountErrorLabel.hidden = true
        passwordTextField.layer.borderColor = UIColor.blackColor().CGColor
        passwordTextField.layer.borderWidth = 0.1
        passErrorLabel.hidden = true
        passwordConfirmTextField.layer.borderColor = UIColor.blackColor().CGColor
        passwordConfirmTextField.layer.borderWidth = 0.1
        confirmPassErrorLabel.hidden = true
        firstNameTextField.layer.borderColor = UIColor.blackColor().CGColor
        firstNameTextField.layer.borderWidth = 0.1
        firstNameErrorLabel.hidden = true
        lastNameTextField.layer.borderColor = UIColor.blackColor().CGColor
        lastNameTextField.layer.borderWidth = 0.1
        lastNameErrorLabel.hidden = true
        phoneTextField.layer.borderColor = UIColor.blackColor().CGColor
        phoneTextField.layer.borderWidth = 0.1
        phoneErrorLabel.hidden = true
        
        validator.validate(self)
    }
    
    func validationSuccessful() {
        let username = accountTextField.text!
        let password = passwordTextField.text!
        let firstName = firstNameTextField.text!
        let lastName = lastNameTextField.text!
        let tel = phoneTextField.text!
        
        let sharedAuth = Auth.sharedAuth
        let signUp = sharedAuth.signUp(username, password: password, firstName: firstName,
            lastName: lastName, tel: tel)
        
        if signUp == 200 {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else if signUp == 500 {
            let alert = UIAlertView(title: "溫馨提醒", message: "不好意思，這個Email已經被註冊過，請登入或者是嘗試其他Email，謝謝...", delegate: self, cancelButtonTitle: "確認")
            alert.show()
        } else if signUp == 300 {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
    
    func validationFailed(errors: [UITextField : ValidationError]) {
        for (field, error) in validator.errors {
            field.layer.borderColor = UIColor.redColor().CGColor
            field.layer.borderWidth = 1.0
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.hidden = false
        }
    }

}
