//
//  Location.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/18/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation
import UIKit
import Just

// A protocol for passing user's location 
// to the Map View.
protocol MyLocationDelegate {
    func moveToMyLocation(myLocation: CLLocationCoordinate2D)
}

class Location: NSObject {
    class var sharedLocation: Location {
        struct Singleton {
            static let instance = Location()
        }
        
        return Singleton.instance
    }
    
    var anotherLocationManager: CLLocationManager! = nil
    var myLocation: CLLocation! = nil
    var delegate: MyLocationDelegate! = nil
    var token: dispatch_once_t = 0
    var distanceConfig = 500.00
    var expiryTimeConfig = 86400.00
    
    func startMonitoringLocation() {
        if anotherLocationManager != nil {
            anotherLocationManager.stopMonitoringSignificantLocationChanges()
        }
        
        anotherLocationManager = CLLocationManager()
        anotherLocationManager.delegate = self
        anotherLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        anotherLocationManager.activityType = CLActivityType.OtherNavigation
        anotherLocationManager.requestAlwaysAuthorization()
        anotherLocationManager.startMonitoringSignificantLocationChanges()
    }
    
    func restartMonitoringLocation() {
        anotherLocationManager.stopMonitoringSignificantLocationChanges()
        anotherLocationManager.requestAlwaysAuthorization()
        anotherLocationManager.startMonitoringSignificantLocationChanges()
    }
}

extension Location: CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {

        if status != .AuthorizedAlways {
            let defaultLocation = CLLocationCoordinate2D(latitude: 25.050725, longitude: 121.51784)
            delegate.moveToMyLocation(defaultLocation)
            
            dispatch_once(&token) {
                let alert = UIAlertView(title: "溫馨提醒", message: "打開定位您可以收到附近的優惠訊息喔！",
                    delegate: self, cancelButtonTitle: "確認")
                alert.show()
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            let newLocation = location
            
            self.myLocation = newLocation
        }
        
        delegate.moveToMyLocation(myLocation.coordinate)
        
        let sharedPoi = PoiModel.sharedPoiModel
        let poiList = sharedPoi.getData()
        
        let appState = UIApplication.sharedApplication().applicationState
        
        if appState == UIApplicationState.Active {
            for poi in poiList {
                getDistance(poi)
            }
        }
    }
    
    /// A method to calculate the distance between each marker 
    /// and user's current location.
    ///
    /// If the distance is less than 500 meters and the notification 
    /// push time is greater than one day, send the notification and 
    /// update the database.
    ///
    /// - Parameters:
    ///   - marker: A `NSDictionary` of POI markers
    func getDistance(marker: NSDictionary) {
        let mLongitude = marker.objectForKey("longitude") as! Double
        let mLatitude = marker.objectForKey("latitude") as! Double
        let mLocation = CLLocation(latitude: mLatitude, longitude: mLongitude)
        
        let distance = self.myLocation.distanceFromLocation(mLocation)
        
        let sharedAuth = Auth.sharedAuth
        if sharedAuth.debug == true {
            distanceConfig = 5000.00
            expiryTimeConfig = 60.00
        }
        
        // To check if the distance is less than 500 meters 
        /// and greater than 0 meter
        if distance <= distanceConfig && distance > 0 {
            let poiId = marker.objectForKey("id") as! NSInteger
            let poiName = marker.objectForKey("name") as! String
            
            let pushDateTimeStr = marker.objectForKey("push_datetime") as! String
            let pushDateTime = NSDate(dateString: pushDateTimeStr)
            let dateInterval = abs(pushDateTime.timeIntervalSinceNow)
            
            if dateInterval >= expiryTimeConfig { // 1 day = 86500 seconds
                let formatter = NSDateFormatter()
                formatter.dateFormat = "yyy-MM-dd HH:mm:ss"
                let nowTime = NSDate()
                let nowTimeStr = formatter.stringFromDate(nowTime)
                
                let sharedPoi = PoiModel.sharedPoiModel
                sharedPoi.updateNotificationTime(poiId, notificationTime: nowTimeStr)
                
                let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
                let deviceId = sharedAuth.apnsToken
                let sharedApi = ApiList.sharedApi
                let apiUrl = sharedApi.getNotificationApi(deviceId)
                
                Just.post(apiUrl, data: ["uid": poiId, "type": 2], headers: ["Authorization": authToken]) { (r) in
                }
                
                let localNotification:UILocalNotification = UILocalNotification()
                localNotification.alertAction = "Testing notifications on iOS8"
                localNotification.alertBody = "「\(poiName)」在您的附近呢！要不要去逛逛？感受一下大螢幕的氛圍！"
                localNotification.fireDate = NSDate(timeIntervalSinceNow: 15)
                localNotification.soundName = UILocalNotificationDefaultSoundName
                localNotification.userInfo = ["poi_id": poiId, "type": 2]
                UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            }
        }

    }
    
}