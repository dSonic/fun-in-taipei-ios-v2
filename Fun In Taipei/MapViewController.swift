//
//  MapViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/10/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Haneke
import Just

class MapViewController: UIViewController, MyLocationDelegate {
    
    private var apiList: ApiList!
    private var poiList: [NSDictionary] = []
    private var myMarker: GMSMarker!
    private var tempMarkers: [PoiMarker] = []
    private var token: dispatch_once_t = 0
    private var token2: dispatch_once_t = 1
    private var typeSettings:Array<Int> = []
    private var locationSettings:Int = 0
    private var locations = FilterSettings.sharedFilterSettings.locations
    private var currentTappedMarker: PoiMarker!
    private var poiDetailInfo: NSDictionary!
    private var sharedLocation = Location.sharedLocation
    
    let locationManager = CLLocationManager()
    var tappedMarker: PoiMarker!
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var filterTitle: UILabel!
    @IBOutlet weak var filterContent: UILabel!
    @IBOutlet weak var filterButton: UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        // Changed the background image of TabBar 
        // to match the view every time the view is loaded.
        self.parentViewController?.tabBarController?.tabBar.backgroundImage = UIImage(named: "tabBar_map")
        
        let filterSettings = FilterSettings.sharedFilterSettings
        var typeSetting:Array<Int> = []
        var typeSettingString:[String] = []
        
        if filterSettings.settings.valueForKey("food") as! Bool {
            typeSetting.append(1)
            typeSettingString.append("美食")
        }
        
        if filterSettings.settings.valueForKey("poi") as! Bool {
            typeSetting.append(3)
            typeSettingString.append("景點")
        }
        
        if filterSettings.settings.valueForKey("shopping") as! Bool {
            typeSetting.append(2)
            typeSettingString.append("購物")
        }
        
        self.typeSettings = typeSetting
        self.locationSettings = filterSettings.settings.valueForKey("location") as! NSInteger
        
        var contentText = self.locations[self.locationSettings]
        
        for typeString in typeSettingString {
            contentText += " \(typeString)"
        }
        
        self.filterContent.textAlignment = NSTextAlignment.Left
        self.filterContent.text = contentText

        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        var count = 0
        
        if self.tempMarkers.count > 0 {
            self.mapView.clear()
            
            for marker in self.tempMarkers {
                if self.typeSettings.contains(marker.poi.valueForKey("type") as! NSInteger) {
                    if self.locationSettings != 0  {
                        let address = marker.poi["address"] as! String
                        
                        if address.containsString(self.locations[self.locationSettings]) {
                            marker.map = self.mapView
                        }
                        
                        if bounds.containsCoordinate(marker.position) {
                            count++
                        }
                    } else {
                        marker.map = self.mapView
                        
                        if bounds.containsCoordinate(marker.position) {
                            count++
                        }
                    }
                }
            }
            
            if count > 0 {
                self.navigationItem.rightBarButtonItem?.enabled = true
            } else {
                self.navigationItem.rightBarButtonItem?.enabled = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.drawSlideMenuButton()
        self.drawPoiListButton()
        
        sharedLocation.delegate = self
        
        mapView.delegate = self
        
        // Setup navbar title image
        let titleImage = UIImage(named: "main_navbar_title.png")!
        self.navigationItem.titleView = UIImageView(image: titleImage)
        
        let mapFilterTap = UITapGestureRecognizer(target: self, action: "mapFilterTapped:")
        mapFilterTap.numberOfTapsRequired = 1
        self.filterButton.addGestureRecognizer(mapFilterTap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
    * Draw a custom Slide Menu Button ont left of the Navigation Bar
    */
    
    func drawSlideMenuButton() {
        let addImage: UIImage? = UIImage(named: "slide_menu_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        
        let slideMenuButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        if self.revealViewController() != nil {
            addButton.addTarget(self.revealViewController(), action: "revealToggle:", forControlEvents: .TouchUpInside)
            self.view.addGestureRecognizer((self.revealViewController().panGestureRecognizer()))
        }
        
        self.navigationItem.setLeftBarButtonItem(slideMenuButton, animated: false)
    }
    
    /**
    * Draw a custom POI List Button on the right of the Navigation Bar
    */
    
    func drawPoiListButton() {
        let addImage: UIImage? = UIImage(named: "map_poi_list_btn.png")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "poiListButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setRightBarButtonItem(poiListButton, animated: false)
    }
    
    // Action metho for PoiList Button on the Navbar
    func poiListButtonPressed(sender: UIButton!) {
        self.performSegueWithIdentifier("MapListPush", sender: sender)
    }
    
    func mapFilterTapped(sender: UIImage) {
        self.performSegueWithIdentifier("MapFilterPush", sender: sender)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MapListPush" {
            let mapListVC = segue.destinationViewController as! MapListTableViewController
            
            let region = self.mapView.projection.visibleRegion()
            let bounds = GMSCoordinateBounds(region: region)
            
            for marker in self.tempMarkers {
                if bounds.containsCoordinate(marker.position) {
                    if self.typeSettings.contains(marker.poi["type"] as! NSInteger) {
                        if self.locationSettings != 0 {
                            let address = marker.poi["address"] as! String
                            if address.containsString(self.locations[self.locationSettings]) {
                                mapListVC.poiList.append(marker)
                            }
                        } else {
                            mapListVC.poiList.append(marker)
                        }
                    }
                }
            }
        } else if segue.identifier == "PoiDetailSegue" {
            let poiDetailVC = segue.destinationViewController as! PoiDetailViewController
            
            poiDetailVC.tappedMarker = tappedMarker
            poiDetailVC.poiDetailInfo = poiDetailInfo
            poiDetailVC.isFavorite = poiDetailInfo.objectForKey("is_favorite") as! Bool
            poiDetailVC.poiId = tappedMarker.poi["id"] as! Int
        }
    }
    
}

extension MapViewController: CLLocationManagerDelegate {
    
    /// A Location Delegate Method for gettting user's 
    /// current location and move the map to it
    ///
    /// If the user decided to not authorize the location service, 
    /// will move the map to a default location.
    ///
    /// Otherwise, call API to get all markers and display them
    /// on the Map.
    ///
    /// - Parameters:
    ///   - myLocation: A `CLLocationCoordinate2D` location corrdinate
    ///
    func moveToMyLocation(myLocation: CLLocationCoordinate2D) {
        mapView.myLocationEnabled = true
        mapView.settings.myLocationButton = true
        //let defaultLocation = CLLocationCoordinate2D(latitude: 25.050725, longitude: 121.51784)
        mapView.camera = GMSCameraPosition(target: myLocation, zoom: 15, bearing: 0, viewingAngle: 0)
        
        // To prevent the method being called multiple times
        dispatch_once(&token) {
            self.getMarkers()
        }
    }
    
    /**
    * Getting Marker Info by calling API
    *
    */
    
    func getMarkers() {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        var count = 0
        
        apiList = ApiList.sharedApi
        let apiUrl = apiList.getApiUrl("poiList")
        
        Alamofire.request(.GET, apiUrl)
            .responseJSON { response in
                if let JSON = response.result.value {
                    for poi in JSON as! [NSDictionary] {
                        let cache = Shared.imageCache
                        
                        let URL = NSURL(string: poi["thumbnail"] as! String)!
                        let fetcher = NetworkFetcher<UIImage>(URL: URL)
                        
                        cache.fetch(fetcher: fetcher).onSuccess { image in
                            // Do something with image
                            let marker = PoiMarker(poi: poi, thumbnail: image)
                            self.tempMarkers.append(marker)
                            
                            // self.getDistance(marker)
                            
                            if self.typeSettings.contains(marker.poi["type"] as! NSInteger) {
                                if self.locationSettings != 0 {
                                    let address = marker.poi["address"] as! String
                                    
                                    if address.containsString(self.locations[self.locationSettings]) {
                                        marker.map = self.mapView
                                    }
                                } else {
                                    marker.map = self.mapView
                                }
                            }
                            
                            if bounds.containsCoordinate(marker.position) {
                                count += 1
                            }
                            
                            if count <= 0 {
                                self.navigationItem.rightBarButtonItem?.enabled = false
                            } else {
                                self.navigationItem.rightBarButtonItem?.enabled = true
                            }
                        }
                    }
                }
        }
    }
    
    func getDistance(marker: PoiMarker) {
        let mLongitude = marker.position.longitude
        let mLatitude = marker.position.latitude
        let mLocation = CLLocation(latitude: mLatitude, longitude: mLongitude)
        
        let distance = locationManager.location?.distanceFromLocation(mLocation)
        
        if distance <= 200 {
            let poiId = marker.poi["id"] as! Int
            let poiName = marker.poi["name"] as! String
            
            let localNotification:UILocalNotification = UILocalNotification()
            localNotification.alertAction = "Testing notifications on iOS8"
            localNotification.alertBody = "經過 \(poiName)"
            localNotification.fireDate = NSDate(timeIntervalSinceNow: 5)
            localNotification.soundName = UILocalNotificationDefaultSoundName
            localNotification.userInfo = ["poi_id": poiId]
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            
            let sharedAuth = Auth.sharedAuth
            let authToken = sharedAuth.authToken.isEmpty == true ? "" : "JWT \(sharedAuth.authToken)"
            let sharedApi = ApiList.sharedApi
            let apiUrl = sharedApi.getApiUrl("send_notification")
            
            Just.post(apiUrl, data: ["uid": poiId, "type": 2], headers: ["Authorization": authToken]) { (r) in
                if r.ok {
                    // do something if necessary
                }
            }
        }
    }
}

extension MapViewController: GMSMapViewDelegate {
    
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        let myMarker = marker as! PoiMarker
        
        if let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView {
            infoView.markerInfoStoreName.text = myMarker.poi["name"] as? String
            
            var typeNameColor = UIColor(netHex: 0x9FD1CC)
            var typeNameTitle = ""
            
            if myMarker.poi["type"] as! NSInteger == 1 {
                typeNameColor = UIColor(netHex: 0xEFB4BE)
                typeNameTitle = "美食"
            } else if myMarker.poi["type"] as! NSInteger == 2 {
                typeNameColor = UIColor(netHex: 0xBDB8D8)
                typeNameTitle = "購物"
            } else if myMarker.poi["type"] as! NSInteger == 3 {
                typeNameColor = UIColor(netHex: 0x9FD1CC)
                typeNameTitle = "景點"
            }
            
            infoView.markerInfoTypeTitle.text = typeNameTitle
            infoView.markerInfoTypeTitle.textColor = typeNameColor
            infoView.markerInfoStoreName.text = myMarker.poi["name"] as? String
            infoView.markerInfoMovieName.text = myMarker.poi["movie_name"] as? String
            
            infoView.markerInfoImage.image = myMarker.thumbnail
            
            return infoView
        } else {
            return nil
        }
    }
    
    func mapView(mapView: GMSMapView!, idleAtCameraPosition position: GMSCameraPosition!) {
        let region = self.mapView.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        var count = 0
        
        for marker in self.tempMarkers {
            if self.typeSettings.contains(marker.poi["type"] as! NSInteger) {
                if self.locationSettings != 0 {
                    let address = marker.poi["address"] as! String
                    
                    if address.containsString(self.locations[self.locationSettings]) {
                        if bounds.containsCoordinate(marker.position) {
                            count += 1
                        }
                    }
                } else {
                    if bounds.containsCoordinate(marker.position) {
                        count += 1
                    }
                }
            }
        }
        
        if count <= 0 {
            self.navigationItem.rightBarButtonItem?.enabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
    }
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        tappedMarker = marker as! PoiMarker
        
        let poiId = tappedMarker.poi["id"] as! Int
        let authToken = Auth.sharedAuth.authToken.isEmpty == true ? "" : "JWT \(Auth.sharedAuth.authToken)"
        let sharedApi = ApiList.sharedApi
        
        let apiUrl = sharedApi.getPoiDetailApi(poiId)
        
        let response = Just.get(
            apiUrl,
            headers: ["Authorization": authToken]
        )
        
        if response.ok {
            poiDetailInfo = response.json?.objectForKey("data") as? NSDictionary
            self.performSegueWithIdentifier("PoiDetailSegue", sender: nil)
        } else if response.statusCode == nil {
            let alert = Alert()
            alert.noConnectionAlert()
        } else {
            let alert = Alert()
            alert.unknownServerErrorAlert()
        }
    }
}
