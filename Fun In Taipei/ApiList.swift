//
//  ApiList.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation
import UIKit
import Just

class ApiList {
    class var sharedApi: ApiList {
        struct Singleton {
            static let instance = ApiList()
        }
        
        return Singleton.instance;
    }
    
    private(set) var apiHost:String
    private(set) var apiUrls:[String:String]
    
    init() {
        apiHost = ""
        apiUrls = [
            "poiList": "/mobile_api/poi/",
            "signin": "/mobile_api/login/",
            "signup": "/mobile_api/register/",
            "forgetPass": "/mobile_api/user/forgot_password/",
            "bookmark": "/mobile_api/device/favorites/",
            "movies": "/mobile_api/movie/",
            "trip_advisor": "/mobile_api/travel_advisor/",
            "send_notification": "/mobile_api/notification/",
            "notification": "/mobile_api/notification/",
            "reset_password": "/mobile_api/user/reset_password/",
            "user_profile": "/mobile_api/user/profile/",
            "feedback": "/mobile_api/feedback/"
        ]
    }
    
    func setUrlHost() {
        //self.apiHost = "http://127.0.0.1:3130"
        
        let r = Just.post(
            "http://vcm.lbstek.com/fqnd/fit/ios/",
            data: ["version": "2.1"]
        )
        
        if (r.ok) {
            let apiUrl = r.json?.valueForKey("data")?.valueForKey("FQND")
            self.apiHost = apiUrl as! String
        }
    }
    
    func getApiUrl(apiName: String) -> String {
        return "\(apiHost)\(self.apiUrls[apiName]!)"
    }
    
    func getIsFavoriteApi(poiId: Int) -> String {
        return "\(apiHost)/mobile_api/poi/\(poiId)/is_favorite/"
    }
    
    func getPoiDetailApi(poiId: Int) -> String {
        return "\(apiHost)/mobile_api/poi/\(poiId)/detail/"
    }
    
    func getCouponApi(couponId: Int) -> String {
        return "\(apiHost)/mobile_api/qrcode/\(couponId)/coupon/"
    }
    
    func getMovieDetailApi(movieId: Int) -> String {
        return "\(apiHost)/mobile_api/movie/\(movieId)/detail/"
    }
    
    func getMovieFilterApi(types: String) -> String {
        if types.isEmpty == true {
            return "\(apiHost)/mobile_api/movie/"
        } else {
            return "\(apiHost)/mobile_api/movie/?types=\(types)"
        }
    }
    
    func getBookmarkFilterApi(types: String, area: Int) -> String {
        if area != 0 {
            return "\(apiHost)/mobile_api/device/favorites/?types=\(types)&area=\(area)"
        } else {
            return "\(apiHost)/mobile_api/device/favorites/?types=\(types)"
        }
    }
    
    func getRatingEditingApi(poiId: Int) -> String {
        return "\(apiHost)/mobile_api/poi/\(poiId)/rating/"
    }
    
    func getNotificationApi(deviceId: String) -> String {
        return "\(apiHost)/mobile_api/notification/\(deviceId)/"
    }
    
    func getAdsApi(uid: Int) -> String {
        return "\(apiHost)/mobile_api/ads/\(uid)/"
    }
}
