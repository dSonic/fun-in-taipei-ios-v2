//
//  PoiPromotionContentViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/8/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol PoiPromotionDelegate {
    func showQRCodeReader()
    func resizeContentSize(height: CGFloat)
}

class PoiPromotionContentViewController: UIViewController {
    var delegate: PoiPromotionDelegate! = nil
    @IBOutlet weak var qrcodeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // calculate the position of the Qrcode Button 
        // and pass it to the delegate
        let height = qrcodeButton.frame.origin.y + 50 + 50
        delegate.resizeContentSize(height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getPromotionButtonPressed(sender: UIButton) {
        let sharedAuth = Auth.sharedAuth
        
        if sharedAuth.authToken.isEmpty == false {
            delegate.showQRCodeReader()
        } else {
            presentSigninAlertView()
        }
    }

    /**
     * This function to show the Login Alert View
     * if the user has pressed the bookmark button without login.
     */
    func presentSigninAlertView() {
        let alertController = UIAlertController(title: "溫馨提醒",
            message: "此功能需要登入後才能使用...", preferredStyle: .Alert)
        let signInAction = UIAlertAction(title: "立即登入", style: .Destructive, handler: {action in
            let authVC = self.storyboard?.instantiateViewControllerWithIdentifier("AuthRootViewController") as! AuthRootViewController
            self.presentViewController(authVC, animated: true, completion: nil)
        })
        
        let cancelAction = UIAlertAction(title: "暫不登入", style: .Cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(signInAction)
        
        presentViewController(alertController, animated: true, completion: nil)
    }
}
