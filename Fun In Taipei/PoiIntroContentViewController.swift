//
//  PoiIntroContentViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 12/4/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol PoiIntroContentDelegate {
    func resizeContentSize(contentHeight: CGFloat)
}

class PoiIntroContentViewController: UIViewController {
    
    var poiDetailInfo: NSDictionary!
    
    @IBOutlet weak var poiImageView: UIImageView!
    @IBOutlet weak var poiImageView2: UIImageView!
    @IBOutlet weak var poiImageView3: UIImageView!
    @IBOutlet weak var poiThumbImg1: UIImageView!
    @IBOutlet weak var poiThumbImg2: UIImageView!
    @IBOutlet weak var poiThumbImg3: UIImageView!
    @IBOutlet weak var poiDescLabel: UILabel!
    
    var poiImages: Array<NSDictionary>!
    var delegate: PoiIntroContentDelegate! = nil
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        poiImages = (poiDetailInfo.objectForKey("images") as! NSArray) as! Array
        
        var count = 0
        
        for poiImage in poiImages {
            let imgUrl = poiImage.objectForKey("image") as! String
            let URL = NSURL(string: imgUrl)!
            
            if count == 0 {
                poiImageView.hnk_setImageFromURL(URL)
                poiThumbImg1.hnk_setImageFromURL(URL)
                
                poiThumbImg1.layer.borderWidth = 2.0
                poiThumbImg1.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            if count == 1 {
                poiImageView2.hnk_setImageFromURL(URL)
                poiThumbImg2.hnk_setImageFromURL(URL)
                
                poiThumbImg2.layer.borderWidth = 2.0
                poiThumbImg2.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            if count == 2 {
                poiImageView3.hnk_setImageFromURL(URL)
                poiThumbImg3.hnk_setImageFromURL(URL)
                
                poiThumbImg3.layer.borderWidth = 2.0
                poiThumbImg3.layer.borderColor = UIColor.whiteColor().CGColor
            }
            
            count = count+1
        }
        
        poiDescLabel.text = poiDetailInfo.objectForKey("description") as? String
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let thumbImgTapRecognizer = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer:"))
        let thumbImgTapRecognizer2 = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer2:"))
        let thumbImgTapRecognizer3 = UITapGestureRecognizer(target: self,
            action:Selector("handleThumbImgTapRecognizer3:"))
        
        poiThumbImg1.addGestureRecognizer(thumbImgTapRecognizer)
        poiThumbImg2.addGestureRecognizer(thumbImgTapRecognizer2)
        poiThumbImg3.addGestureRecognizer(thumbImgTapRecognizer3)
    }
    
    override func viewDidLayoutSubviews() {
        let contentSize = poiDescLabel.sizeThatFits(poiDescLabel.bounds.size)
        var frame = poiDescLabel.frame
        frame.size.height = contentSize.height
        poiDescLabel.frame = frame
        
        // Resize the content size based on the contents in the child view.
        let height = poiDescLabel.frame.origin.y + poiDescLabel.frame.size.height + 50
        delegate.resizeContentSize(height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleThumbImgTapRecognizer(sender: UITapGestureRecognizer) {
        poiImageView2.hidden = true
        poiImageView3.hidden = true
        
        poiImageView.hidden = false
    }
    
    func handleThumbImgTapRecognizer2(sender: UITapGestureRecognizer) {
        poiImageView.hidden = true
        poiImageView3.hidden = true
        
        poiImageView2.hidden = false
    }
    
    func handleThumbImgTapRecognizer3(sender: UITapGestureRecognizer) {
        poiImageView.hidden = true
        poiImageView2.hidden = true
        
        poiImageView3.hidden = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
