//
//  PoiMarker.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/11/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit
import Haneke
import SQLite

class PoiMarker: GMSMarker {
    let poi: NSDictionary
    let thumbnail: UIImage!
    
    init(poi: NSDictionary, thumbnail: UIImage) {
        self.poi = poi
        self.thumbnail = thumbnail
        
        super.init()
        
        position.latitude = (poi["latitude"] as! NSString).doubleValue
        position.longitude = (poi["longitude"] as! NSString).doubleValue
        
        var imageName: String = ""
        let poiType = poi["type"] as! NSInteger
        
        if poiType == 1 {
            imageName = "pin_food.png"
        } else if poiType == 2 {
            imageName = "pin_store.png"
        } else if poiType == 3 {
            imageName = "pin_attraction.png"
        }
        
        icon = UIImage(named: imageName)!
        groundAnchor = CGPoint(x: 0, y: 1)
        infoWindowAnchor = CGPoint(x: 0, y: -0.2)
        appearAnimation = kGMSMarkerAnimationPop
    }
}
