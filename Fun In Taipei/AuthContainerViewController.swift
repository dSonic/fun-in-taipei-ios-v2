//
//  AuthContainerViewController.swift
//  Fun In Taipei
//
//  Created by LBSTek Inc. on 11/24/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class AuthContainerViewController: UIViewController, SignInViewDelegate, SignUpViewDelegate, ForgetPassViewDelegate {
    private var signInVC: SignInViewController!
    private var signUpVC: SignUpViewController!
    private var forgetPassVC: ForgetPassViewController!
    private var rootVC: AuthRootViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC!.didMoveToParentViewController(self)
        //signInVC.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchFromSignInToSignUp() {
        signInVC.willMoveToParentViewController(nil)
        signInVC.view.removeFromSuperview()
        signInVC.removeFromParentViewController()
        
        signUpVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        signUpVC.view.frame = self.view.frame
        self.addChildViewController(signUpVC)
        self.view.insertSubview(signUpVC.view, atIndex: 0)
        signUpVC.didMoveToParentViewController(self)
        signUpVC.delegate = self
    }
    
    func switchFromSignInToForgetPass() {
        signInVC.willMoveToParentViewController(nil)
        signInVC.view.removeFromSuperview()
        signInVC.removeFromParentViewController()
        
        forgetPassVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgetPassViewController") as! ForgetPassViewController
        forgetPassVC.view.frame = self.view.frame
        self.addChildViewController(forgetPassVC)
        self.view.insertSubview(forgetPassVC.view, atIndex: 0)
        forgetPassVC.didMoveToParentViewController(self)
        forgetPassVC.delegate = self
    }

    func switchFromSignUpToSignIn() {
        signUpVC.willMoveToParentViewController(nil)
        signUpVC.view.removeFromSuperview()
        signUpVC.removeFromParentViewController()
        
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC.didMoveToParentViewController(self)
        //signInVC.delegate = self
    }
    
    func switchFromSignUpToForgetPass() {
        signUpVC.willMoveToParentViewController(nil)
        signUpVC.view.removeFromSuperview()
        signUpVC.removeFromParentViewController()
        
        forgetPassVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgetPassViewController") as! ForgetPassViewController
        forgetPassVC.view.frame = self.view.frame
        self.addChildViewController(forgetPassVC)
        self.view.insertSubview(forgetPassVC.view, atIndex: 0)
        forgetPassVC.didMoveToParentViewController(self)
        forgetPassVC.delegate = self
    }
    
    func switchFromForgetPassToSignIn() {
        forgetPassVC.willMoveToParentViewController(nil)
        forgetPassVC.view.removeFromSuperview()
        forgetPassVC.removeFromParentViewController()
        
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC.didMoveToParentViewController(self)
        //signInVC.delegate = self
    }
    
    func switchFromForgetPassToSignUp() {
        forgetPassVC.willMoveToParentViewController(nil)
        forgetPassVC.view.removeFromSuperview()
        forgetPassVC.removeFromParentViewController()
        
        signUpVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        signUpVC.view.frame = self.view.frame
        self.addChildViewController(signUpVC)
        self.view.insertSubview(signUpVC.view, atIndex: 0)
        signUpVC.didMoveToParentViewController(self)
        signUpVC.delegate = self
    }
}
