//
//  AuthRootViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/18/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

class AuthRootViewController: UIViewController, SignInViewDelegate, SignUpViewDelegate, ForgetPassViewDelegate {
    private var signInVC: SignInViewController!
    private var signUpVC: SignUpViewController!
    private var forgetPassVC: ForgetPassViewController!
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var myScrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
    
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC!.didMoveToParentViewController(self)
        signInVC.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //myScrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.size.width, 600)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawCancelButton() {
        let addImage: UIImage? = UIImage(named: "map_filter_cancel_btn")
        let addButton: UIButton = UIButton(type: .Custom)
        
        addButton.frame = CGRectMake(0, 0, addImage!.size.width, addImage!.size.height)
        addButton.setBackgroundImage(addImage, forState: .Normal)
        addButton.addTarget(self, action: "cancelButtonPressed:", forControlEvents: .TouchUpInside)
        
        let poiListButton: UIBarButtonItem = UIBarButtonItem(customView: addButton)
        
        self.navigationItem.setLeftBarButtonItem(poiListButton, animated: false)
    }
    
    func cancelButtonPressed(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func switchFromSignInToSignUp() {
        signInVC.willMoveToParentViewController(nil)
        signInVC.view.removeFromSuperview()
        signInVC.removeFromParentViewController()
        
        signUpVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        signUpVC.view.frame = self.view.frame
        self.addChildViewController(signUpVC)
        self.view.insertSubview(signUpVC.view, atIndex: 0)
        signUpVC.didMoveToParentViewController(self)
        signUpVC.delegate = self
    }
    
    func switchFromSignInToForgetPass() {
        signInVC.willMoveToParentViewController(nil)
        signInVC.view.removeFromSuperview()
        signInVC.removeFromParentViewController()
        
        forgetPassVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgetPassViewController") as! ForgetPassViewController
        forgetPassVC.view.frame = self.view.frame
        self.addChildViewController(forgetPassVC)
        self.view.insertSubview(forgetPassVC.view, atIndex: 0)
        forgetPassVC.didMoveToParentViewController(self)
        forgetPassVC.delegate = self
    }
    
    func switchFromSignUpToSignIn() {
        signUpVC.willMoveToParentViewController(nil)
        signUpVC.view.removeFromSuperview()
        signUpVC.removeFromParentViewController()
        
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC.didMoveToParentViewController(self)
        signInVC.delegate = self
    }

    func switchFromSignUpToForgetPass() {
        signUpVC.willMoveToParentViewController(nil)
        signUpVC.view.removeFromSuperview()
        signUpVC.removeFromParentViewController()
        
        forgetPassVC = self.storyboard?.instantiateViewControllerWithIdentifier("ForgetPassViewController") as! ForgetPassViewController
        forgetPassVC.view.frame = self.view.frame
        self.addChildViewController(forgetPassVC)
        self.view.insertSubview(forgetPassVC.view, atIndex: 0)
        forgetPassVC.didMoveToParentViewController(self)
        forgetPassVC.delegate = self
    }
    
    func switchFromForgetPassToSignIn() {
        forgetPassVC.willMoveToParentViewController(nil)
        forgetPassVC.view.removeFromSuperview()
        forgetPassVC.removeFromParentViewController()
        
        signInVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        signInVC.view.frame = self.view.frame
        self.addChildViewController(signInVC)
        self.view.insertSubview(signInVC.view, atIndex: 0)
        signInVC.didMoveToParentViewController(self)
        signInVC.delegate = self
    }
    
    func switchFromForgetPassToSignUp() {
        forgetPassVC.willMoveToParentViewController(nil)
        forgetPassVC.view.removeFromSuperview()
        forgetPassVC.removeFromParentViewController()
        
        signUpVC = self.storyboard?.instantiateViewControllerWithIdentifier("SignUpViewController") as! SignUpViewController
        signUpVC.view.frame = self.view.frame
        self.addChildViewController(signUpVC)
        self.view.insertSubview(signUpVC.view, atIndex: 0)
        signUpVC.didMoveToParentViewController(self)
        signUpVC.delegate = self
    }

}
