//
//  PoiModel.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 11/13/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import Foundation
import UIKit
import SQLite
import Alamofire
import Just

class PoiModel {
    class var sharedPoiModel: PoiModel {
        struct Singleton {
            static let instance = PoiModel()
        }
        
        return Singleton.instance
    }
    
    let dbName = "db_fit.sqlite3"

    init() {
        //self.generateData()
    }
    
    func generateData() {
        let dir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let path = dir.stringByAppendingPathComponent(dbName)
        
        let db = try! Connection(path)
            
        let poiTable = Table("poi")
        
        let id = Expression<NSInteger>("id")
        let name = Expression<String>("name")
        let type = Expression<NSInteger>("type")
        let thumbnail = Expression<String>("thumbnail")
        let longitude = Expression<Double>("longitude")
        let latitude = Expression<Double>("latitude")
        let movie_name = Expression<String>("movie_name")
        let movie_year = Expression<String>("movie_year")
        let address = Expression<String>("address")
        let area = Expression<NSInteger>("area")
        let movie_id = Expression<NSInteger>("movie_id")
        let push_datetime = Expression<String>("push_datetime")
        
        try! db.run(poiTable.create(ifNotExists: true) { t in
                t.column(id, primaryKey: true)
                t.column(name)
                t.column(type)
                t.column(thumbnail)
                t.column(longitude)
                t.column(latitude)
                t.column(movie_name)
                t.column(movie_year)
                t.column(address)
                t.column(area)
                t.column(movie_id)
                t.column(push_datetime)
            })
        
        let apiUrl = ApiList.sharedApi.getApiUrl("poiList")
        let requestResponse = Just.get(apiUrl)
        
        if requestResponse.ok {
            let responseData = requestResponse.json as! [NSDictionary]
            
            for poi in responseData {
                let poiId = (poi["id"] as! NSNumber).integerValue
                let poiName = poi["name"] as! String
                let poiType = (poi["type"] as! NSNumber).integerValue
                let poiThumb = poi["thumbnail"] as! String
                let poiLongitude = (poi["longitude"] as! NSString).doubleValue
                let poiLatitude = (poi["latitude"] as! NSString).doubleValue
                let poiMovieName = poi["movie_name"] as! String
                let poiMovieYear = poi["movie_year"] as! String
                let poiAddress = poi["address"] as! String
                let poiArea = poi["area"] as! NSInteger
                let poiMovieId = poi["movie_id"] as! NSInteger
                let pushDateTime = "1979-9-12 9:12:12"
                
                do {
                    try db.run(poiTable.insert(id <- poiId, name <- poiName, longitude <- poiLongitude, latitude <- poiLatitude, movie_name <- poiMovieName, type <- poiType, thumbnail <- poiThumb,
                        movie_year <- poiMovieYear, address <- poiAddress, area <- poiArea, movie_id <- poiMovieId, push_datetime <- pushDateTime))
                } catch _ {
                    let poiRow = poiTable.filter(id == poiId)
                    
                    try!db.run(poiRow.update(name <- poiName, longitude <- poiLongitude, latitude <- poiLatitude, movie_name <- poiMovieName, type <- poiType, thumbnail <- poiThumb,
                        movie_year <- poiMovieYear, address <- poiAddress, area <- poiArea, movie_id <- poiMovieId))
                }
            }
        }
        
    }
    
    func getData() -> [NSDictionary] {
        var apiList:[NSDictionary] = []
        
        let dir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let path = dir.stringByAppendingPathComponent(dbName)
        
        let db = try! Connection(path)
        let poiTable = Table("poi")
        
        let count = db.scalar(poiTable.count)
        
        if count > 0 {
            let id = Expression<NSInteger>("id")
            let name = Expression<String>("name")
            let type = Expression<NSInteger>("type")
            let thumbnail = Expression<String>("thumbnail")
            let longitude = Expression<Double>("longitude")
            let latitude = Expression<Double>("latitude")
            let movie_name = Expression<String>("movie_name")
            let movie_year = Expression<String>("movie_year")
            let address = Expression<String>("address")
            let area = Expression<NSInteger>("area")
            let movie_id = Expression<NSInteger>("movie_id")
            let push_datetime = Expression<String>("push_datetime")
            
            for poi in try!db.prepare(poiTable) {
                let poiDict = [
                    "id": poi[id],
                    "name": poi[name],
                    "type": poi[type],
                    "thumbnail": poi[thumbnail],
                    "longitude": poi[longitude],
                    "latitude": poi[latitude],
                    "movie_name": poi[movie_name],
                    "movie_year": poi[movie_year],
                    "address": poi[address],
                    "area": poi[area],
                    "movie_id": poi[movie_id],
                    "push_datetime": poi[push_datetime]
                ]
                
                apiList.append(poiDict)
            }
        }
        
        return apiList
    }
    
    func updateNotificationTime(poiId: NSInteger, notificationTime: String) {
        let dir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let path = dir.stringByAppendingPathComponent(dbName)
        
        let db = try! Connection(path)
        let poiTable = Table("poi")
        
        let id = Expression<NSInteger>("id")
        let push_datetime = Expression<String>("push_datetime")
        
        let poi = poiTable.filter(id == poiId)
        
        try! db.run(poi.update(push_datetime <- notificationTime))
    }
    
    func getNotificationMessageByPoiId(poiId: Int) -> String {
        let dir = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let path = dir.stringByAppendingPathComponent(dbName)
        
        let db = try! Connection(path)
        let poiTable = Table("poi")
        
        let id = Expression<NSInteger>("id")
        let name = Expression<String>("name")
        
        let poiQuery = poiTable.select(name).filter(id == poiId)
        
        var poiName = ""
        
        if let poi = db.pluck(poiQuery) {
            poiName = poi[name]
        }
        
        return "「\(poiName)」在您的附近呢！要不要去逛逛？感受一下大銀幕的氛圍！"
    }
}