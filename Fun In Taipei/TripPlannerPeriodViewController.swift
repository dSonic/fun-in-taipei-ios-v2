//
//  TripPlannerPeriodViewController.swift
//  Fun In Taipei
//
//  Created by LBStek Inc. on 12/14/15.
//  Copyright © 2015 LBSTek Inc. All rights reserved.
//

import UIKit

protocol TripPlannerPeriodDelegate {
    func moveFromDaysToWeather()
    func confirmButtonToggleFromPeriod()
}

class TripPlannerPeriodViewController: UIViewController {
    @IBOutlet weak var halfDayButton: UIButton!
    @IBOutlet weak var oneDayButton: UIButton!
    @IBOutlet weak var twoDayButton: UIButton!
    
    private var buttonImages: Array<UIImage> = [
        UIImage(named: "trip_planner_half_day_btn")!,
        UIImage(named: "trip_planner_one_day_btn")!,
        UIImage(named: "trip_planner_two_day_btn")!,
    ]
    
    private var buttonPressedImages: Array<UIImage> = [
        UIImage(named: "trip_planner_half_day_btn_pressed")!,
        UIImage(named: "trip_planner_one_day_btn_pressed")!,
        UIImage(named: "trip_planner_two_day_btn_pressed")!,
    ]
    
    private let sharedSettings = TripPlannerSettings.sharedTripPlannerSettings
    private var settings: Dictionary<String,Int>!
    
    var delegate: TripPlannerPeriodDelegate! = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        settings = sharedSettings.settings as! Dictionary
        
        let imageIndex = settings["period"]!
        
        var buttonImage: UIImage!
        var selectedButton: UIButton!
        
        switch imageIndex {
        case 0:
            buttonImage = buttonPressedImages[imageIndex]
            selectedButton = halfDayButton
        case 1:
            buttonImage = buttonPressedImages[imageIndex]
            selectedButton = oneDayButton
        case 2:
            buttonImage = buttonPressedImages[imageIndex]
            selectedButton = twoDayButton
        default:
            selectedButton = nil
        }
        
        if selectedButton != nil {
            selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
        }
    }
    
    private func switchToButton(sender: UIButton) {
        let buttonImage = buttonPressedImages[sender.tag - 1]
        var selectedButton: UIButton!
        
        switch sender.tag {
        case 1:
            selectedButton = halfDayButton
            oneDayButton.setBackgroundImage(buttonImages[1], forState: .Normal)
            twoDayButton.setBackgroundImage(buttonImages[2], forState: .Normal)
        case 2:
            selectedButton = oneDayButton
            halfDayButton.setBackgroundImage(buttonImages[0], forState: .Normal)
            twoDayButton.setBackgroundImage(buttonImages[2], forState: .Normal)
        case 3:
            selectedButton = twoDayButton
            halfDayButton.setBackgroundImage(buttonImages[0], forState: .Normal)
            oneDayButton.setBackgroundImage(buttonImages[1], forState: .Normal)
        default:
            selectedButton = halfDayButton
            oneDayButton.setBackgroundImage(buttonImages[1], forState: .Normal)
            twoDayButton.setBackgroundImage(buttonImages[2], forState: .Normal)
        }
        
        selectedButton.setBackgroundImage(buttonImage, forState: .Normal)
    }

    @IBAction func halfDatButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["period"] = 0
        sharedSettings.updateSettings(settings)
        delegate.moveFromDaysToWeather()
        delegate.confirmButtonToggleFromPeriod()
    }
    
    @IBAction func oneDayButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["period"] = 1
        sharedSettings.updateSettings(settings)
        delegate.moveFromDaysToWeather()
        delegate.confirmButtonToggleFromPeriod()
    }

    @IBAction func twoDayButtonPressed(sender: UIButton) {
        switchToButton(sender)
        settings["period"] = 2
        sharedSettings.updateSettings(settings)
        delegate.moveFromDaysToWeather()
        delegate.confirmButtonToggleFromPeriod()
    }
    
}
